Require Export Coch.Cochis.
Require Import List.
Import ListNotations.

(* Refined grammar of types *)
Inductive CTy : Set :=
  | ctall (ρ : CTy)
  | ciarr (ρ1 ρ2 : CTy)
  | sty (τ : STy)
with STy : Set :=
  | stvar (x : nat)
  | starr (ρ1 ρ2 : CTy).

Scheme CTy_ind' := Induction for CTy  Sort Prop
  with STy_ind' := Induction for STy Sort Prop.

Combined Scheme CTy_STy_ind
  from CTy_ind', STy_ind'.

(* Inclusion functions to ambiguous Cochis types and vice versa propositions *)
Fixpoint CTy2Ty (ρ : CTy) : Ty :=
  match ρ with
  | ctall ρ'    => tall (CTy2Ty ρ')
  | ciarr ρ1 ρ2 => tiarr (CTy2Ty ρ1) (CTy2Ty ρ2)
  | sty τ       => STy2Ty τ
  end
with STy2Ty (τ : STy) : Ty :=
  match τ with
  | stvar x     => tvar x
  | starr ρ1 ρ2 => tarr (CTy2Ty ρ1) (CTy2Ty ρ2)
  end.

Definition CTy2FTy (ρ : CTy) : F.Ty :=
  CtoFTy (CTy2Ty ρ).

Fixpoint MTy2STy (σ : MTy) : STy :=
  match σ with
  | mtvar x     => stvar x
  | mtarr σ1 σ2 => starr (sty (MTy2STy σ1)) (sty (MTy2STy σ2))
  end.

Lemma MTy2STy_inj : 
  forall σ σ', MTy2STy σ = MTy2STy σ' -> σ = σ'.
Proof.
intro σ. induction σ; intro σ'; destruct σ'; simpl;
intro Heq; inversion Heq; f_equal; auto.
Qed.

Lemma MTy2STy_STy2Ty:
  forall σ,
    (STy2Ty (MTy2STy σ)) = MTy2Ty σ.
Proof.
  intro σ; induction σ; simpl; auto.
  rewrite IHσ1, IHσ2; reflexivity.
Qed.

Inductive DEnv : Set :=
  | dempty
  | detvar (Γ : DEnv)
  | devar  (Γ : DEnv) (ρ : CTy)
  | deimpl (Γ : DEnv) (ρ : CTy).

Fixpoint appendDEnv (Γ1 Γ2 : DEnv) : DEnv :=
  match Γ2 with
  | dempty       => Γ1
  | detvar Γ2'   => detvar (appendDEnv Γ1 Γ2')
  | devar Γ2' ρ  => devar (appendDEnv Γ1 Γ2') ρ
  | deimpl Γ2' ρ => deimpl (appendDEnv Γ1 Γ2') ρ
  end.

Lemma appendDEnv_dempty_left : forall Γ,
  appendDEnv dempty Γ = Γ.
Proof.
intros. induction Γ; simpl; f_equal; trivial.
Qed.

Lemma appendDEnv_assoc : forall (Γ1 Γ2 Γ3 : DEnv),
  (appendDEnv (appendDEnv Γ1 Γ2) Γ3) = appendDEnv Γ1 (appendDEnv Γ2 Γ3).
Proof.
intros. induction Γ3; simpl; f_equal; trivial.
Qed.

Fixpoint DEnv2Env (Γ: DEnv): Env :=
  match Γ with
  | dempty      => empty
  | detvar Γ'   => etvar (DEnv2Env Γ')
  | devar  Γ' ρ  => evar  (DEnv2Env Γ') (CTy2Ty ρ)
  | deimpl Γ' ρ => eimpl (DEnv2Env Γ') (CTy2Ty ρ)
  end.

Lemma appendDEnv_DEnv2Env_comm (Γ1 Γ2 : DEnv) :
  appendEnv (DEnv2Env Γ1) (DEnv2Env Γ2) = DEnv2Env (appendDEnv Γ1 Γ2).
Proof.
induction Γ2; simpl; f_equal; auto.
Qed.

Inductive shift_deimpl : Ren -> DEnv -> DEnv -> Prop :=
  | shift_deimpl_here (G : DEnv) (ρ : CTy): shift_deimpl S G (deimpl G ρ)
  | shift_deimpl_there_evar (r : Ren) (G1 G2 : DEnv) (ρ : CTy) :
      shift_deimpl r G1 G2 ->
      shift_deimpl (ren_up r) (devar G1 ρ) (devar G2 ρ)
  | shift_deimpl_there_etvar (r : Ren) (G1 G2 : DEnv) :
      shift_deimpl r G1 G2 ->
      shift_deimpl r (detvar G1) (detvar G2)
  | shift_deimpl_there_eimplD (r : Ren) (G1 G2 : DEnv) (ρ : CTy) :
      shift_deimpl r G1 G2 ->
      shift_deimpl (ren_up r) (deimpl G1 ρ) (deimpl G2 ρ).

Lemma shift_deimpl2shift_eimpl (r : Ren) (Γ1 Γ2 : DEnv) 
  (sh : shift_deimpl r Γ1 Γ2) : shift_eimpl r (DEnv2Env Γ1) (DEnv2Env Γ2).
Proof.
induction sh; simpl; constructor; auto.
Qed.

(* Type renaming and commutativity lemmata *)
Fixpoint renCTy (ρ : CTy) (ξ : nat -> nat) : CTy :=
  match ρ with
  | ctall ρ'    => ctall (renCTy ρ' (ren_up ξ))
  | ciarr ρ1 ρ2 => ciarr (renCTy ρ1 ξ) (renCTy ρ2 ξ)
  | sty τ       => sty (renSTy τ ξ)
  end
with renSTy (τ : STy) (ξ : nat -> nat) : STy :=
  match τ with
  | stvar x     => stvar (ξ x)
  | starr ρ1 ρ2 => starr (renCTy ρ1 ξ) (renCTy ρ2 ξ)
  end.

Lemma renCSTy_comp:
  (forall ρ ξ₁ ξ₂, renCTy (renCTy ρ ξ₁) ξ₂ = renCTy ρ (ren_comp ξ₁ ξ₂)) /\
  (forall τ ξ₁ ξ₂, renSTy (renSTy τ ξ₁) ξ₂ = renSTy τ (ren_comp ξ₁ ξ₂)).
Proof.
apply CTy_STy_ind; intros; simpl; f_equal; eauto.
rewrite H. f_equal. dbruijn. trivial.
Qed.

Lemma renCTy_comp:
  forall ρ ξ₁ ξ₂, renCTy (renCTy ρ ξ₁) ξ₂ = renCTy ρ (ren_comp ξ₁ ξ₂).
Proof. apply renCSTy_comp. Qed.

Lemma renSTy_comp:
  forall τ ξ₁ ξ₂, renSTy (renSTy τ ξ₁) ξ₂ = renSTy τ (ren_comp ξ₁ ξ₂).
Proof. apply renCSTy_comp. Qed.

Lemma renMTy_S_inj : 
  forall σ σ', renMTy σ S = renMTy σ' S -> σ = σ'.
Proof.
intro σ. induction σ; intro σ'; destruct σ'; simpl;
intro Heq; inversion Heq; f_equal; auto.
Qed.

Lemma MTy2STy_renMTy : forall ρ r,
  MTy2STy (renMTy ρ r) = renSTy (MTy2STy ρ) r.
Proof.
intro ρ. induction ρ; intro r; simpl; repeat f_equal; eauto.
Qed.

Lemma CTy2Ty_renCTy_comm : forall ρ ζ, 
  CTy2Ty (renCTy ρ ζ) = renTy (CTy2Ty ρ) ζ
with STy2Ty_renSTy_comm : forall τ ζ,
  STy2Ty (renSTy τ ζ) = renTy (STy2Ty τ) ζ.
Proof.
induction ρ; simpl; intro; f_equal; eauto.
* induction τ; simpl.
  - reflexivity.
  - intro. f_equal; eauto.
Qed.

Definition weakenDMTy (Γ : DEnv) (σ : MTy) : MTy :=
  weakenMTy (DEnv2Env Γ) σ.

Fixpoint weakenCTy (Γ : DEnv) (ρ : CTy) : CTy :=
  match Γ with
  | dempty       => ρ
  | detvar Γ'    => renCTy (weakenCTy Γ' ρ) S
  | devar Γ' ρ'  => weakenCTy Γ' ρ
  | deimpl Γ' ρ' => weakenCTy Γ' ρ
  end.

Lemma CTy2Ty_weakenCTy : forall Γ ρ,
  CTy2Ty (weakenCTy Γ ρ) = weakenTy (DEnv2Env Γ) (CTy2Ty ρ).
Proof.
intro Γ. induction Γ as [ | Γ IHΓ | Γ ρ' IHΓ' | Γ ρ' IHΓ'];
intro ρ; simpl; auto. rewrite CTy2Ty_renCTy_comm. f_equal. auto.
Qed.

Fixpoint weakenSTy (Γ : DEnv) (τ : STy) : STy :=
  match Γ with
  | dempty      => τ
  | detvar Γ'   => renSTy (weakenSTy Γ' τ) S
  | devar Γ' _  => weakenSTy Γ' τ
  | deimpl Γ' _ => weakenSTy Γ' τ
  end.

Lemma STy2Ty_weakenSTy : forall Γ τ,
  STy2Ty (weakenSTy Γ τ) = weakenTy (DEnv2Env Γ) (STy2Ty τ).
Proof.
intro Γ. induction Γ as [ | Γ IHΓ | Γ ρ' IHΓ' | Γ ρ' IHΓ'];
intro ρ; simpl; auto. rewrite STy2Ty_renSTy_comm. f_equal. auto.
Qed.

Lemma MTy2STy_weaken : forall Γ σ,
  MTy2STy (weakenDMTy Γ σ) = weakenSTy Γ (MTy2STy σ).
Proof.
intros a. unfold weakenDMTy. induction a; intros; simpl; trivial.
rewrite MTy2STy_renMTy. f_equal. trivial.
Qed.

Lemma weakenCTy_appendDEnv : ∀ (Γ1 Γ2 : DEnv),
   (∀ (ρ : CTy),  weakenCTy (appendDEnv Γ1 Γ2) ρ = weakenCTy Γ2 (weakenCTy Γ1 ρ)).
Proof.
induction Γ2; intros ρ'; simpl; auto. f_equal. auto.
Qed.

(* Type substitution *)

Fixpoint subCTy (ρ : CTy) (ζ: SubMT) : CTy :=
  match ρ with 
  | ctall ρ'    => ctall (subCTy ρ' (submt_up ζ))
  | ciarr ρ1 ρ2 => ciarr (subCTy ρ1 ζ) (subCTy ρ2 ζ)
  | sty τ       => sty (subSTy τ ζ)
  end
with subSTy (τ : STy) (ζ: SubMT) : STy :=
  match τ with
  | stvar x     => MTy2STy (ζ x)
  | starr ρ1 ρ2 => starr (subCTy ρ1 ζ) (subCTy ρ2 ζ)
  end.



Lemma subSTy_MTy2STy : forall σ s,
  (subSTy (MTy2STy σ) s = MTy2STy (subMTy σ s)).
Proof.
intro σ; induction σ; simpl; intro s; repeat f_equal; trivial.
Qed.

Lemma submt_inst_compCS :
  (forall ρ ζ₁ ζ₂, subCTy (subCTy ρ ζ₁) ζ₂ = subCTy ρ (submt_comp ζ₁ ζ₂)) /\
  (forall τ ζ₁ ζ₂, subSTy (subSTy τ ζ₁) ζ₂ = subSTy τ (submt_comp ζ₁ ζ₂)).
Proof.
apply CTy_STy_ind; simpl; intros; f_equal; auto.
- rewrite <- submt_comp_submt_up. auto.
- rewrite subSTy_MTy2STy. trivial.
Qed.

Lemma submt_inst_compC : forall ρ ζ₁ ζ₂,
  subCTy (subCTy ρ ζ₁) ζ₂ = subCTy ρ (submt_comp ζ₁ ζ₂).
Proof. apply submt_inst_compCS. Qed.

Lemma submt_inst_compS : forall τ ζ₁ ζ₂,
  subSTy (subSTy τ ζ₁) ζ₂ = subSTy τ (submt_comp ζ₁ ζ₂).
Proof. apply submt_inst_compCS. Qed.

Lemma CTySTy2Ty_subCTySTy :
  (forall ρ s, CTy2Ty (subCTy ρ s) = subTy (CTy2Ty ρ) s)
  /\ 
  (forall τ s, STy2Ty (subSTy τ s) = subTy (STy2Ty τ) s).
Proof.
apply CTy_STy_ind.
- intros ρ Hs s. simpl. rewrite Hs. trivial.
- intros ρ Hc ρ' Hc' s. simpl. rewrite Hc, Hc'. trivial.
- intros τ Hs s. simpl. trivial.
- intros x s. simpl. rewrite MTy2STy_STy2Ty. trivial.
- intros ρ Hc ρ' Hc' s. simpl. rewrite Hc, Hc'. trivial.
Qed.

Lemma CTy2Ty_subCTy : forall ρ s,
  CTy2Ty (subCTy ρ s) = subTy (CTy2Ty ρ) s.
Proof. apply CTySTy2Ty_subCTySTy. Qed.

Lemma subCTySTy_submt_id:
  (forall ρ, subCTy ρ submt_id = ρ) /\ (forall τ, subSTy τ submt_id = τ).
Proof.
apply CTy_STy_ind; intros; simpl;
f_equal; try rewrite submt_id_up; auto.
Qed.

Lemma subCTy_submt_id:
  forall ρ,  subCTy ρ submt_id = ρ.
Proof.
  apply subCTySTy_submt_id.
Qed.

Lemma subSTy_submt_id:
  forall τ, subSTy τ submt_id = τ.
Proof.
  apply subCTySTy_submt_id.
Qed.

Definition MtoFSubTy (ζ : nat -> MTy) : nat -> F.Ty :=
  fun i => CtoFTy (STy2Ty (MTy2STy(ζ i))).

Lemma MtoFSubTy_submt_id:
  MtoFSubTy submt_id = F.subt_id.
Proof.
  apply functional_extensionality; intro n; induction n;
  unfold MtoFSubTy, F.subt_id; simpl; reflexivity.
Qed.

Lemma renCSTy_to_subCSTy : 
  (∀ ρ s, renCTy ρ s  = subCTy ρ (ren_to_submt s)) /\
  (∀ τ s, renSTy τ s  = subSTy τ (ren_to_submt s)).
Proof.
unfold ren_to_submt. apply CTy_STy_ind; intros; simpl; f_equal; auto.
rewrite H. f_equal. extensionality i. destruct i; auto.
Qed.

Lemma renCTy_to_subCTy :
  ∀ ρ s, renCTy ρ s  = subCTy ρ (ren_to_submt s).
Proof. apply renCSTy_to_subCSTy. Qed.

Lemma renSTy_to_subSTy : 
  ∀ τ s, renSTy τ s  = subSTy τ (ren_to_submt s).
Proof. apply renCSTy_to_subCSTy. Qed.

Definition weakenDSubMT (Γ : DEnv) (s : SubMT) :=
  weakenSubMT (DEnv2Env Γ) s.

Definition weakenRenTyD (Γ : DEnv) (r : Ren) :=
  weakenRenTy (DEnv2Env Γ) r.

Lemma weakenCTy_subCTy : forall Γ ρ s,
  weakenCTy Γ (subCTy ρ s) = subCTy (weakenCTy Γ ρ) (weakenDSubMT Γ s).
Proof.
intro Γ. induction Γ; simpl; trivial; unfold shiftT, shift.
intros. rewrite IHΓ. unfold weakenDSubMT. simpl.
repeat rewrite renCTy_to_subCTy, submt_inst_compC.
f_equal. rewrite submt_comp_S. trivial.
Qed.

Definition restrict {A : Set} (a : nat) (s : nat -> A) : nat -> A :=
  fun i => match (i < a) with
           | True => s i
(*            | False => i *)
           end.

(* nat represents the lower bound of substitutable types *)
Inductive MSubMT : nat -> DEnv -> SubMT -> Prop :=
  | nosubt (a : nat) (Γ : DEnv) : MSubMT a Γ submt_id
  | conssubt a Γ σ s θ Γ1 Γ2 :
      submt_etvar (DEnv2Env Γ) σ s (DEnv2Env Γ1) (DEnv2Env Γ2) ->
      restrict (S a) s = submt_id ->
      MSubMT (S a) Γ2 θ ->
      MSubMT a Γ1 (submt_comp s θ).

Fixpoint subtDEnv (Γ : DEnv) (s : SubMT) : DEnv :=
  match Γ with
  | dempty      => dempty
  | detvar Γ'   => detvar (subtDEnv Γ' s)
  | devar Γ' ρ  => devar (subtDEnv Γ' s) (subCTy ρ (weakenDSubMT Γ' s))
  | deimpl Γ' ρ => deimpl (subtDEnv Γ' s) (subCTy ρ (weakenDSubMT Γ' s))
  end.

Lemma subtDEnv_DEnv2Env : forall Γ s,
  DEnv2Env (subtDEnv Γ s) = subtEnv (DEnv2Env Γ) s.
Proof.
intro Γ. induction Γ; intros s; simpl; repeat f_equal; trivial;
rewrite CTy2Ty_subCTy; repeat f_equal.
Qed.

Inductive DRes_match : DEnv -> CTy -> F.Tm -> STy -> F.Tm -> list CTy -> Prop :=
  | M_Simp Γ τ E:
          DRes_match Γ (sty τ) E τ E []
  | M_Iapp Γ ρ1 ρ2 E τ E' Σ:
          DRes_match (deimpl Γ ρ1) ρ2 (F.app (F.shift E) (F.var 0)) τ E' Σ
       -> DRes_match Γ (ciarr ρ1 ρ2) E τ E' (ρ1::Σ)
  | M_Tapp Γ ρ σ  E τ E' Σ:
          DRes_match Γ (subCTy ρ (snoc submt_id σ)) (F.tapp E (CtoFTy (MTy2Ty σ))) τ E' Σ
       -> wfTy (DEnv2Env Γ) (MTy2Ty σ)
       -> DRes_match Γ (ctall ρ) E τ E' Σ.

Inductive DRes_focus (a : nat) : DEnv -> CTy -> F.Tm -> Prop :=
  | R_IAbs Γ ρ1 ρ2 E : 
          DRes_focus a (deimpl Γ ρ1) ρ2 E 
       -> DRes_focus a Γ (ciarr ρ1 ρ2) (F.abs (CtoFTy (CTy2Ty ρ1)) E)
  | R_TAbs Γ ρ E : 
          DRes_focus (S a) (detvar Γ) ρ E
       -> DRes_focus a Γ (ctall ρ) (F.tabs E)
  | R_Simpl Γ τ E : 
          DRes_lookup a dempty Γ τ E 
       -> DRes_focus a Γ (sty τ) E

with DRes_lookup (a : nat) : DEnv -> DEnv -> STy -> F.Tm -> Prop :=
  | L_Match ρ Δ Γ' E E' τ Σ :
          DRes_match (appendDEnv (deimpl Γ' ρ) Δ) (weakenCTy Δ ρ) (F.weakenTm (CtoFEnv (DEnv2Env Δ)) (F.var 0)) τ E Σ
       -> Sigma_dres_focus a (appendDEnv (deimpl Γ' ρ) Δ) Σ E E' (* Helper for L-RuleMatch quantified premise *)
       -> DRes_lookup a Δ (deimpl Γ' ρ) τ E'
  | L_NoMatch Δ ρ Γ' E τ r:
          stable a (appendDEnv Γ' Δ) ρ τ
       -> DRes_lookup a Δ Γ' τ E
       -> shift_eimpl r (DEnv2Env (appendDEnv Γ' Δ)) (DEnv2Env (appendDEnv (deimpl Γ' ρ) Δ))
       -> DRes_lookup a Δ (deimpl Γ' ρ) τ (F.renTm E F.ren_id r)
  | L_Var Δ ρ Γ' E τ :
          DRes_lookup a (appendDEnv (devar dempty ρ) Δ) Γ' τ E
       -> DRes_lookup a Δ (devar Γ' ρ) τ E
  | L_TVar Δ Γ' E τ :
          DRes_lookup a (appendDEnv (detvar dempty) Δ) Γ' τ E
       -> DRes_lookup a Δ (detvar Γ') τ E

with Sigma_dres_focus (a : nat) : DEnv -> list CTy -> F.Tm -> F.Tm -> Prop :=
  | base Γ E : 
          Sigma_dres_focus a Γ [] E E
  | dres_cons Γ (ρ : CTy) (l : list CTy) E1 E2 E' :
          DRes_focus a Γ ρ E'
       -> Sigma_dres_focus a (deimpl Γ ρ) l E1 E2
       -> Sigma_dres_focus a Γ (ρ::l) E1 (F.subTm E2 F.subt_id (F.snoc F.sub_id E'))

with stable (a : nat) : DEnv -> CTy -> STy -> Prop :=
  | one (Γ : DEnv) (ρ : CTy) (τ : STy) : not (
     exists (s : SubMT) (E : F.Tm) (Σ : list CTy),
     MSubMT a Γ s ->
     DRes_match (subtDEnv Γ s) (subCTy ρ s) (F.var 0) (subSTy τ s) E Σ) ->
     stable a Γ ρ τ.

Inductive DRes (Γ : DEnv) (ρ : CTy) (E : F.Tm) : Prop :=
  | R_Main : DRes_focus 0 Γ ρ E -> DRes Γ ρ E.

Fixpoint impl_varsD' (ls : list CTy) : DEnv :=
  match ls with
  | []     => dempty
  | ρ::ls' => deimpl (impl_varsD' ls') ρ
  end.

Definition impl_varsD (ls : list CTy) : DEnv :=
  impl_varsD' (rev ls).

Lemma appendDEnv_impl_varsD : forall a b,
  impl_varsD (a ++ b) =
  appendDEnv (impl_varsD a) (impl_varsD b).
Proof.
intros a b. revert a. induction b as [ | b' b IHb]; intro a; simpl.
- rewrite app_nil_r. trivial.
- change (b' :: b) with ([b'] ++ b).
  rewrite app_assoc.
  repeat rewrite IHb.
  rewrite <- appendDEnv_assoc. simpl. f_equal.
  unfold impl_varsD. rewrite rev_app_distr. simpl. trivial.
Qed.

Ltac fix_impl_varD :=
  match goal with
  | [ |- context[impl_varsD (?a :: ?l)]] =>
      change (a :: l) with ([a] ++ l);
      rewrite appendDEnv_impl_varsD; unfold impl_varsD;
      simpl; fold (impl_varsD l)
  | [ H : context[impl_varsD (?a :: ?l)] |- _ ] =>
      change (a :: l) with ([a] ++ l) in H;
      rewrite appendDEnv_impl_varsD in H; unfold impl_varsD in H;
      simpl in H; fold (impl_varsD l) in H
  end.

Lemma impl_vars_DEnv2Env : forall Σ,
  DEnv2Env (impl_varsD Σ) = impl_vars (map CTy2Ty Σ).
Proof.
intro. induction Σ; simpl. trivial.
fix_impl_varD. fix_impl_vars.
rewrite <- appendDEnv_DEnv2Env_comm. f_equal. trivial.
Qed.

Lemma weakenSTy_appendDEnv_assoc : forall (Γ Δ : DEnv) (τ : STy),
  weakenSTy (appendDEnv Γ Δ) τ = weakenSTy Δ (weakenSTy Γ τ).
Proof.
intros. revert Γ. induction Δ; intros; simpl; trivial;
rewrite IHΔ; f_equal.
Qed.

Lemma impl_varsD_weakenSTy_cancel : forall (τ : STy) (Σ : list CTy),
  weakenSTy (impl_varsD Σ) τ = τ.
Proof.
intros. revert τ. induction Σ; intros; simpl; trivial.
fix_impl_varD. rewrite weakenSTy_appendDEnv_assoc.
  rewrite IHΣ. induction τ; simpl; trivial.
Qed.

(* Soundness Lemmas *)

Fixpoint weakenRenDEnvTy (Γ : DEnv) : Ren :=
  match Γ with
  | dempty      => ren_id
  | detvar Γ'   => ren_comp (weakenRenDEnvTy Γ') S
  | devar Γ' _  => weakenRenDEnvTy Γ'
  | deimpl Γ' _ => weakenRenDEnvTy Γ'
  end.

Lemma DRes_match_impl_vars :
  forall (Γ : DEnv) (ρ : CTy) (E E' : F.Tm) (τ : STy) (Σ : list CTy),
  DRes_match Γ ρ E τ E' Σ ->
  amb_res (DEnv2Env Γ) (CTy2Ty ρ) E ->
  amb_res (DEnv2Env (appendDEnv Γ (impl_varsD Σ))) (STy2Ty (weakenSTy (impl_varsD Σ) τ)) E'.
Proof.
intros Γ ρ E E' τ Σ dr. induction dr; intros ar.
- simpl in *. trivial.
- fix_impl_varD. repeat rewrite <- appendDEnv_assoc. simpl.
  rewrite weakenSTy_appendDEnv_assoc.
  rewrite impl_varsD_weakenSTy_cancel in *.
  rewrite STy2Ty_weakenSTy. simpl.
  apply IHdr; clear IHdr. simpl. econstructor.
  + unfold F.shift.
    pose proof shift_eimpl_here (DEnv2Env Γ) (CTy2Ty ρ1) as sh.
    pose proof (shift_eimpl_amb_res _ _ _ ar _ _ sh) as K. simpl in *.
    eauto.
  + constructor. constructor.
- apply IHdr. clear IHdr. simpl in ar.
  (* AR_Tapp *)
  pose proof shift_etvar_here (DEnv2Env Γ) as sh1.
  pose proof shift_etvar_amb_res (DEnv2Env Γ) _ E ar _ _ sh1 as Shar. 
  clear ar. simpl in Shar.
(*   pose proof wf_tvar (etvar (DEnv2Env Γ)) 0 lookup_etvar_here as wf. *)
(*   pose proof AR_Tapp. *)
  pose proof AR_Tapp _ _ _ (mtvar 0) Shar as Shar2. clear Shar. dbruijn. isimpl.
  pose proof subt_etvar_here (DEnv2Env Γ) σ as se1.
  pose proof (submt_etvar_amb_res) _ _
    (appendEnv (etvar (DEnv2Env Γ)) empty)
    (appendEnv (DEnv2Env Γ) (subtEnv empty (snoc submt_id σ)))
    (weakenSubMT empty (snoc submt_id σ)) se1
    as Sear. simpl in Sear. translation.
  specialize (Sear _ _ Shar2). clear dr Σ τ Shar2. dbruijn. isimpl.
  rewrite CTy2Ty_subCTy. trivial.
Qed.

(* Lemma A.9 *)
Lemma DRes_match_Soundness (Γ : DEnv) (ρ : CTy) (E : F.Tm) :
  forall (E' : F.Tm) (τ : STy) (Σ : list CTy),
  DRes_match Γ ρ E τ E' Σ -> 
  amb_res (DEnv2Env Γ) (CTy2Ty ρ) E -> forall E0,
  Sigma_amb_res (DEnv2Env Γ) (map CTy2Ty Σ) E' E0 ->
  amb_res (DEnv2Env Γ) (STy2Ty τ) E0.
Proof.
intros.
eapply Sigma_amb_res_amb_res; eauto.
rewrite <- impl_vars_DEnv2Env.
repeat rewrite appendDEnv_DEnv2Env_comm.
rewrite <- STy2Ty_weakenSTy.
eapply DRes_match_impl_vars; eauto.
Qed.

Scheme DRes_focus_min := Minimality for DRes_focus Sort Prop
with DRes_lookup_min := Minimality for DRes_lookup Sort Prop
with Sigma_dres_focus_min := Minimality for Sigma_dres_focus Sort Prop.

Combined Scheme DRes_min from
  DRes_focus_min, DRes_lookup_min, Sigma_dres_focus_min.

Lemma DRes_focus_lookup_sigmafocus_Soundness : forall a,
  (forall Γ ρ E, DRes_focus a Γ ρ E -> amb_res (DEnv2Env Γ) (CTy2Ty ρ) E)
  /\
  (forall Δ Γ' τ E, DRes_lookup a Δ Γ' τ E -> amb_res (DEnv2Env (appendDEnv Γ' Δ)) (STy2Ty τ) E)
  /\
  (forall Γ Σ E E', Sigma_dres_focus a Γ Σ E E' -> Sigma_amb_res (DEnv2Env Γ) (map CTy2Ty Σ) E E').
Proof.
apply DRes_min; intros; try constructor; trivial; simpl.
- pose proof DRes_match_Soundness _ _ _ _ _ _ H as K.
  apply K; auto. rewrite <- appendDEnv_DEnv2Env_comm.
  simpl. rewrite CTy2Ty_weakenCTy.
  apply amb_res_weaken. repeat constructor.
- eapply shift_eimpl_amb_res; eauto.
- rewrite <- appendDEnv_assoc in H0. simpl in H0. trivial.
- rewrite <- appendDEnv_assoc in H0. simpl in H0. trivial.
Qed.

Lemma DRes_lookup_Soundness : forall a Δ Γ' τ E,
  DRes_lookup a Δ Γ' τ E ->
    amb_res (DEnv2Env (appendDEnv Γ' Δ)) (STy2Ty τ) E.
Proof. apply DRes_focus_lookup_sigmafocus_Soundness. Qed.

Lemma DRes_focus_Soundness :
  forall a Γ ρ E,
    DRes_focus a Γ ρ E -> amb_res (DEnv2Env Γ) (CTy2Ty ρ) E.
Proof. apply DRes_focus_lookup_sigmafocus_Soundness. Qed.

(* Lemma 5.2 *)
Lemma DRes_Soundness :
  forall Γ ρ E,
    DRes Γ ρ E -> amb_res (DEnv2Env Γ) (CTy2Ty ρ) E.
Proof.
induction 1.
eauto using DRes_focus_Soundness.
Qed.

Definition CtxEq (Γ : Env) (t1 t2 : Tm) (ρ : Ty) (E1 E2 : F.Tm) : Prop :=
  Typing Γ t1 ρ E1 /\ Typing Γ t2 ρ E1 /\
  forall (M : EC (CtoFEnv Γ) (CtoFTy ρ) F.empty F.tunit),
    M{{E1}} ~~ M{{E2}}.