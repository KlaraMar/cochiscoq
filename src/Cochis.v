Require Export Coq.Program.Equality.
Require Export Coq.Program.Tactics.
Require Export Coq.Logic.FunctionalExtensionality.
Require Export Coch.systemFman.
Require Import List.
Import ListNotations.

Module F.
  Include systemFman.
End F.

Section Renamings.

Definition snoc {A: Type} (ζ: nat → A) (x: A) : nat → A :=
  fun i => match i with
             | O   => x
             | S i => ζ i
           end.

Definition ren_comp (ξ₁ ξ₂: Ren) : Ren := fun i => ξ₂ (ξ₁ i).

Definition ren_up (ξ: Ren) : Ren := snoc (ren_comp ξ S) 0.

Lemma ren_id_up : ren_up ren_id = ren_id.
Proof. extensionality i; destruct i; reflexivity. Qed.

Lemma ren_comp_up ξ₁ ξ₂ : ren_comp (ren_up ξ₁) (ren_up ξ₂) = ren_up (ren_comp ξ₁ ξ₂).
Proof. extensionality i; destruct i; reflexivity. Qed.

End Renamings.
Hint Rewrite ren_S_snoc ren_id_up ren_comp_up : infraC.

Ltac dbruijn := simpl in *; autorewrite with infraC in *.

Ltac translation := simpl in *; autorewrite with trans_comm in *.

Section Types.

Inductive MTy : Set :=
  | mtvar (x : nat)
  | mtarr (σ1 σ2 : MTy).

Inductive Ty : Set :=
  | tvar (x : nat)
  | tarr (ρ1 ρ2 : Ty )
  | tall (ρ : Ty)
  | tiarr (ρ1 ρ2 : Ty).

Fixpoint MTy2Ty (σ : MTy) : Ty :=
  match σ with
  | mtvar x     => tvar x
  | mtarr σ1 σ2 => tarr (MTy2Ty σ1) (MTy2Ty σ2)
  end.

Fixpoint CtoFTy (ρ : Ty) : F.Ty :=
  match ρ with
  | tvar x      => F.tvar x
  | tarr ρ1 ρ2  => F.tarr (CtoFTy ρ1) (CtoFTy ρ2)
  | tall ρ      => F.tall (CtoFTy ρ)
  | tiarr ρ1 ρ2 => F.tarr (CtoFTy ρ1) (CtoFTy ρ2)
  end.

Section TypeRenaming.

Fixpoint renTy (ρ : Ty) (ξ : nat -> nat) : Ty :=
  match ρ with
  | tvar x      => tvar (ξ x)
  | tarr ρ1 ρ2  => tarr (renTy ρ1 ξ) (renTy ρ2 ξ)
  | tall ρ'     => tall (renTy ρ' (ren_up ξ))
  | tiarr ρ1 ρ2 => tiarr (renTy ρ1 ξ) (renTy ρ2 ξ)
  end.

Fixpoint renMTy (σ : MTy) (ξ : nat -> nat) : MTy :=
  match σ with
  | mtvar x      => mtvar (ξ x)
  | mtarr σ₁ σ₂  => mtarr (renMTy σ₁ ξ) (renMTy σ₂ ξ)
  end.

Lemma renMTy_renTy : forall σ r,
  MTy2Ty (renMTy σ r) = renTy (MTy2Ty σ) r.
Proof.
intros σ r. induction σ; simpl; f_equal; auto.
Qed.

Lemma MTy2Ty_renMTy : forall σ r,
  MTy2Ty (renMTy σ r) = renTy (MTy2Ty σ) r.
Proof.
intros. induction σ; dbruijn; f_equal; auto.
Qed.

Lemma CtoFTy_renTy_comm (ρ : Ty) : forall (ξ : nat -> nat),
CtoFTy (renTy ρ ξ) = F.renTy (CtoFTy ρ) ξ.
Proof.
induction ρ; simpl; intros; f_equal; auto.
Qed.

Lemma renTy_id ρ : renTy ρ ren_id = ρ.
Proof.
induction ρ; simpl; auto; f_equal; dbruijn; auto.
Qed.

Lemma renTy_comp ρ : forall (ξ₁ ξ₂ : Ren),
  renTy (renTy ρ ξ₁) ξ₂ = renTy ρ (ren_comp ξ₁ ξ₂).
Proof.
induction ρ; intros; simpl; f_equal; auto.
rewrite IHρ. dbruijn. reflexivity.
Qed.

Lemma renMTy_comp ρ : forall (ξ₁ ξ₂ : Ren),
  renMTy (renMTy ρ ξ₁) ξ₂ = renMTy ρ (ren_comp ξ₁ ξ₂).
Proof.
induction ρ; intros; simpl; f_equal; auto.
Qed.

End TypeRenaming.

End Types.
Hint Rewrite renTy_id renTy_comp renMTy_comp : infraC.
Hint Rewrite renMTy_renTy CtoFTy_renTy_comm : trans_comm.

(* Type substitution *)
Section TypeSubstitution.

Definition SubMT : Set := nat → MTy.

Definition CtoFSubT (ζ : SubMT) : nat -> F.Ty :=
  fun i => CtoFTy (MTy2Ty (ζ i)).

Definition submt_id : SubMT := mtvar.

Lemma CtoFSubT_subt_id :
  CtoFSubT submt_id = F.subt_id.
Proof.
unfold CtoFSubT, subt_id, F.subt_id. reflexivity.
Qed.

Lemma CtoFSubT_snoc (ζ : SubMT) (σ : MTy) : 
  CtoFSubT (snoc ζ σ) = F.snoc (CtoFSubT ζ) (CtoFTy (MTy2Ty σ)).
Proof.
unfold CtoFSubT, snoc, F.snoc.
extensionality i. destruct i; translation; reflexivity.
Qed.

Definition submt_up (ζ: SubMT) : SubMT :=
  fun i => match i with
             | O   => mtvar O
             | S i => renMTy (ζ i) S
           end.

Lemma submt_id_up : submt_up submt_id = submt_id.
Proof. extensionality i; destruct i; reflexivity. Qed.
Hint Rewrite submt_id_up : infraC.

Lemma CtoFSubT_submt_up : forall (ζ : SubMT),
   F.subt_up (CtoFSubT ζ) = CtoFSubT (submt_up ζ).
Proof.
intros.
extensionality i. unfold CtoFSubT. destruct i; translation; auto.
Qed.
Hint Rewrite CtoFSubT_submt_up : trans_comm.

Fixpoint subTy (T : Ty) (ζ: SubMT) : Ty :=
  match T with
    | tvar X      => MTy2Ty (ζ X)
    | tarr T1 T2  => tarr (subTy T1 ζ) (subTy T2 ζ)
    | tall T      => tall (subTy T (submt_up ζ))
    | tiarr T1 T2 => tiarr (subTy T1 ζ) (subTy T2 ζ)
  end.

Fixpoint subMTy (σ : MTy) (s : SubMT) : MTy :=
  match σ with
  | mtvar x     => s x
  | mtarr σ1 σ2 => mtarr (subMTy σ1 s) (subMTy σ2 s)
  end.

Lemma MTy2Ty_subMTy : forall σ s,
  MTy2Ty (subMTy σ s) = subTy (MTy2Ty σ) s.
Proof.
intros. induction σ; dbruijn; f_equal; auto.
Qed.

Lemma CtoFTy_subTy (ρ : Ty) : forall (ζ : SubMT),
CtoFTy (subTy ρ ζ) = F.subTy (CtoFTy ρ) (CtoFSubT ζ).
Proof.
induction ρ; intros; simpl; f_equal; translation; auto.
Qed.
Hint Rewrite MTy2Ty_subMTy CtoFTy_subTy : trans_comm.

Lemma subTy_id t : subTy t submt_id = t.
Proof.
induction t; simpl; f_equal; dbruijn; auto.
Qed.

Lemma subMTy_id t : subMTy t submt_id = t.
Proof.
induction t; simpl; f_equal; dbruijn; auto.
Qed.
Hint Rewrite subTy_id subMTy_id : infraC.

Definition submt_comp (ζT₁ ζT₂: SubMT) : SubMT := fun i => subMTy (ζT₁ i) ζT₂.

Lemma CtoFSubT_comp : forall (s1 s2 : SubMT),
  CtoFSubT (submt_comp s1 s2) = F.subt_comp (CtoFSubT s1) (CtoFSubT s2).
Proof.
intros. unfold submt_comp, F.subt_comp, CtoFSubT.
extensionality i. translation. f_equal.
Qed.

Lemma submt_comp_id_left ξ : submt_comp submt_id ξ = ξ.
Proof. extensionality i; destruct i; reflexivity. Qed.
Hint Rewrite submt_comp_id_left : infraC.

Lemma submt_comp_id_right ξ : submt_comp ξ submt_id = ξ.
Proof.
extensionality i; unfold submt_comp. dbruijn. trivial.
Qed.

Lemma submt_comp_snoc (ζ₁ ζ₂ : SubMT) σ :
submt_comp (snoc ζ₁ σ) ζ₂ = snoc (submt_comp ζ₁ ζ₂) (subMTy σ ζ₂).
Proof. extensionality i; destruct i; reflexivity. Qed.

Definition ren_to_submt (ξ: Ren) : SubMT := fun i => mtvar (ξ i).

Lemma ren_id_to_submt : ren_to_submt ren_id = submt_id.
Proof. reflexivity. Qed.

Lemma ren_up_to_submt_up ξ : ren_to_submt (ren_up ξ) = submt_up (ren_to_submt ξ).
Proof. extensionality i; destruct i; reflexivity. Qed.
Hint Rewrite ren_id_to_submt ren_up_to_submt_up : infraC.

Lemma renTy_to_subTy t : ∀ ξ, renTy t ξ  = subTy t (ren_to_submt ξ).
Proof.
unfold ren_to_submt. induction t; intro ξ; dbruijn; f_equal; eauto.
rewrite IHt. f_equal. extensionality i. destruct i; auto.
Qed.

Lemma renMTy_to_subMTy t : ∀ ξ, renMTy t ξ  = subMTy t (ren_to_submt ξ).
Proof.
unfold ren_to_submt. induction t; intro ξ; dbruijn; f_equal; eauto.
Qed.

Lemma snoc_ren_to_submt : forall r x,
  ren_to_submt (snoc r x) = snoc (ren_to_submt r) (mtvar x).
Proof.
intros. unfold ren_to_subt, snoc. extensionality i.
destruct i; trivial.
Qed.

Lemma submt_comp_ren_to_submt : forall ξ1 ξ2,
  submt_comp (ren_to_submt ξ1) (ren_to_submt ξ2) = ren_to_submt (ren_comp ξ1 ξ2).
Proof. reflexivity. Qed.

Lemma submt_S_snoc : snoc (ren_to_submt S) (mtvar 0) = submt_id.
Proof. extensionality i; destruct i; reflexivity. Qed.

Lemma submt_eta ζ : snoc (subt_comp (ren_to_subt S) ζ) (ζ 0) = ζ.
Proof. extensionality i; destruct i; reflexivity. Qed.

Lemma submt_reflection ζ s : submt_comp (ren_to_submt S) (snoc ζ s) = ζ.
Proof. extensionality i; destruct i; reflexivity. Qed.
Hint Rewrite submt_S_snoc submt_eta submt_reflection : infraC.

Lemma submt_comp_ren_submt_up ξ ζ :
submt_comp (submt_up (ren_to_submt ξ)) (submt_up ζ)
  = submt_up (submt_comp (ren_to_submt ξ) ζ).
Proof. extensionality i; destruct i; reflexivity. Qed.

Hint Rewrite subTy_id subMTy_id submt_comp_id_left 
  submt_comp_id_right renTy_to_subTy renMTy_to_subMTy
  ren_up_to_submt_up submt_comp_ren_submt_up : infraC.

Lemma submt_comp_ren_sub s : ∀ ξ ζ,
subTy (subTy s (ren_to_submt ξ)) ζ = subTy s (submt_comp (ren_to_submt ξ) ζ).
Proof.
induction s; intros ξ ζ; dbruijn; f_equal; auto.
rewrite <- ren_up_to_submt_up, IHs. dbruijn. reflexivity.
Qed.

Lemma submt_compM_ren_sub s : ∀ ξ ζ,
subMTy (subMTy s (ren_to_submt ξ)) ζ = subMTy s (submt_comp (ren_to_submt ξ) ζ).
Proof.
induction s; intros ξ ζ; dbruijn; f_equal; auto.
Qed.

Lemma submt_comp_submt_ren_up ξ ζ :
submt_comp (submt_up ζ) (submt_up (ren_to_submt ξ))
  = submt_up (submt_comp ζ (ren_to_submt ξ)).
Proof.
unfold submt_comp. extensionality i; destruct i; dbruijn.
f_equal. repeat rewrite submt_compM_ren_sub. f_equal.
Qed.

Lemma submt_comp_submt_ren s : ∀ ζ ξ,
  subTy (subTy s ζ) (ren_to_submt ξ) = subTy s (submt_comp ζ (ren_to_submt ξ)).
Proof.
induction s; simpl; intros; f_equal; dbruijn; auto.
- rewrite <- MTy2Ty_subMTy. f_equal.
- rewrite <- ren_up_to_submt_up, IHs. dbruijn.
  rewrite submt_comp_submt_ren_up; reflexivity.
Qed.

Lemma submt_compM_submt_ren s : ∀ ζ ξ,
  subMTy (subMTy s ζ) (ren_to_submt ξ) = subMTy s (submt_comp ζ (ren_to_submt ξ)).
Proof.
induction s; simpl; intros; f_equal; dbruijn; auto.
Qed.

Lemma submt_up_def ζ :
  submt_up ζ = snoc (submt_comp ζ (ren_to_submt S)) (mtvar 0).
Proof.
extensionality i; destruct i; dbruijn; auto.
Qed.

Lemma submt_comp_submt_up ζ₁ ζ₂:
  submt_comp (submt_up ζ₁) (submt_up ζ₂) = submt_up (submt_comp ζ₁ ζ₂).
Proof.
unfold subt_comp. extensionality i; destruct i; auto.
- unfold submt_comp. dbruijn. rewrite submt_compM_ren_sub.
  rewrite submt_compM_submt_ren. f_equal.
  rewrite submt_up_def. rewrite submt_reflection.
  reflexivity.
Qed.

Lemma submt_inst_comp s : ∀ ζ₁ ζ₂,
  subTy (subTy s ζ₁) ζ₂ = subTy s (submt_comp ζ₁ ζ₂).
Proof.
induction s; simpl; intros; f_equal; auto.
- rewrite <- MTy2Ty_subMTy. f_equal.
- rewrite IHs, submt_comp_submt_up; reflexivity.
Qed.

Lemma submt_inst_compM s : ∀ ζ₁ ζ₂,
  subMTy (subMTy s ζ₁) ζ₂ = subMTy s (submt_comp ζ₁ ζ₂).
Proof.
induction s; simpl; intros; f_equal; auto.
Qed.

Lemma submt_comp_assoc ξ₁ ξ₂ ξ₃ :
submt_comp (submt_comp ξ₁ ξ₂) ξ₃ = submt_comp ξ₁ (submt_comp ξ₂ ξ₃).
Proof.
extensionality i; repeat unfold submt_comp; simpl.
rewrite submt_inst_compM; reflexivity.
Qed.

Lemma submt_comp_up_id ζ T :
submt_comp (submt_up ζ) (snoc submt_id T) = snoc ζ T.
Proof.
unfold submt_comp. extensionality i; destruct i; dbruijn; auto.
rewrite submt_inst_compM. dbruijn. reflexivity.
Qed.
End TypeSubstitution.
Hint Rewrite submt_id_up subTy_id subMTy_id subTy_id submt_S_snoc submt_comp_id_left 
  submt_comp_id_right renTy_to_subTy renMTy_to_subMTy submt_eta submt_reflection
  ren_up_to_submt_up submt_comp_ren_submt_up submt_comp_ren_sub submt_compM_ren_sub
  submt_up_def submt_inst_comp submt_inst_compM submt_comp_assoc submt_comp_up_id 
  ren_id_to_submt submt_comp_snoc MTy2Ty_renMTy : infraC.
Hint Rewrite CtoFSubT_subt_id CtoFSubT_snoc MTy2Ty_subMTy
  CtoFTy_subTy CtoFSubT_submt_up CtoFSubT_comp : trans_comm.

(* Environment and lookups *)
Section Environment.
Inductive Env : Set :=
  | empty
  | etvar (Γ : Env)
  | evar  (Γ : Env) (ρ : Ty)
  | eimpl (Γ : Env) (ρ : Ty).

Fixpoint CtoFEnv (Γ : Env) : F.Env :=
  match Γ with
  | empty      => F.empty
  | etvar Γ'   => F.etvar (CtoFEnv Γ')
  | evar  Γ' ρ => F.evar (CtoFEnv Γ') (CtoFTy ρ)
  | eimpl Γ' ρ => F.evar (CtoFEnv Γ') (CtoFTy ρ)
  end.

Fixpoint appendEnv (d1 d2 : Env) :=
  match d2 with
  | empty       => d1
  | etvar d2'   => etvar (appendEnv d1 d2')
  | evar d2' ρ  => evar (appendEnv d1 d2') ρ
  | eimpl d2' ρ => eimpl (appendEnv d1 d2') ρ
  end.

Lemma appendEnv_assoc : forall (Γ1 Γ2 Γ3 : Env),
  (appendEnv (appendEnv Γ1 Γ2) Γ3) = appendEnv Γ1 (appendEnv Γ2 Γ3).
Proof.
intros. induction Γ3; simpl; f_equal; trivial.
Qed.

Lemma appendEnv_empty : forall Γ,
  appendEnv empty Γ = Γ.
Proof.
intro. induction Γ; simpl; trivial; rewrite IHΓ; trivial.
Qed.

Lemma CtoFEnv_appendEnv : forall Γ1 Γ2,
  F.appendEnv (CtoFEnv Γ1) (CtoFEnv Γ2) = CtoFEnv (appendEnv Γ1 Γ2).
Proof.
intros. induction Γ2; simpl; f_equal; trivial.
Qed.

Inductive shift_etvar : Ren -> Env -> Env -> Prop :=
  | shift_etvar_here (G : Env) : shift_etvar S G (etvar G)
  | shift_etvar_there_etvar (r : Ren) (G1 G2 : Env) :
      shift_etvar r G1 G2 ->
      shift_etvar (ren_up r) (etvar G1) (etvar G2)
  | shift_etvar_there_evar (r : Ren) (G1 G2 : Env) (T : Ty) :
      shift_etvar r G1 G2 ->
      shift_etvar r (evar G1 T) (evar G2 (renTy T r))
  | shift_etvar_there_eimpl (r : Ren) (G1 G2 : Env) (T : Ty) :
      shift_etvar r G1 G2 ->
      shift_etvar r (eimpl G1 T) (eimpl G2 (renTy T r)).

Inductive shift_evar : Ren -> Env -> Env -> Prop :=
  | shift_evar_here (G : Env) (ρ : Ty): shift_evar S G (evar G ρ)
  | shift_evar_there_evar (r : Ren) (G1 G2 : Env) (ρ : Ty) :
      shift_evar r G1 G2 ->
      shift_evar (ren_up r) (evar G1 ρ) (evar G2 ρ)
  | shift_evar_there_etvar (r : Ren) (G1 G2 : Env) :
      shift_evar r G1 G2 ->
      shift_evar r (etvar G1) (etvar G2)
  | shift_evar_there_eimpl (r : Ren) (G1 G2 : Env) (ρ : Ty) :
      shift_evar r G1 G2 ->
      shift_evar (ren_up r) (eimpl G1 ρ) (eimpl G2 ρ).

Inductive shift_eimpl : Ren -> Env -> Env -> Prop :=
  | shift_eimpl_here (G : Env) (ρ : Ty): shift_eimpl S G (eimpl G ρ)
  | shift_eimpl_there_evar (r : Ren) (G1 G2 : Env) (ρ : Ty) :
      shift_eimpl r G1 G2 ->
      shift_eimpl (ren_up r) (evar G1 ρ) (evar G2 ρ)
  | shift_eimpl_there_etvar (r : Ren) (G1 G2 : Env) :
      shift_eimpl r G1 G2 ->
      shift_eimpl r (etvar G1) (etvar G2)
  | shift_eimpl_there_eimpl (r : Ren) (G1 G2 : Env) (ρ : Ty) :
      shift_eimpl r G1 G2 ->
      shift_eimpl (ren_up r) (eimpl G1 ρ) (eimpl G2 ρ).

Inductive submt_etvar (Γ : Env) (σ : MTy) : SubMT -> Env -> Env -> Prop :=
  | subt_etvar_here : submt_etvar Γ σ (snoc submt_id σ) (etvar Γ) Γ
  | subt_etvar_there_etvar Γ1 Γ2 s:
      submt_etvar Γ σ s Γ1 Γ2 ->
      submt_etvar Γ σ (submt_up s) (etvar Γ1) (etvar Γ2)
  | subt_etvar_there_evar Γ1 Γ2 s ρ':
      submt_etvar Γ σ s Γ1 Γ2 ->
      submt_etvar Γ σ s (evar Γ1 ρ') (evar Γ2 (subTy ρ' s))
  | subt_etvar_there_eimpl Γ1 Γ2 s ρ':
      submt_etvar Γ σ s Γ1 Γ2 ->
      submt_etvar Γ σ s (eimpl Γ1 ρ') (eimpl Γ2 (subTy ρ' s)).
End Environment.
Hint Constructors shift_etvar shift_evar shift_eimpl submt_etvar.

Section Weakening.
Section TypeWeakening.
Fixpoint weakenTy (Γ : Env) (ρ : Ty) : Ty :=
  match Γ with
  | empty       => ρ
  | etvar Γ'    => renTy (weakenTy Γ' ρ) S
  | evar Γ' ρ'  => weakenTy Γ' ρ
  | eimpl Γ' ρ' => weakenTy Γ' ρ
  end.

Fixpoint weakenMTy (Γ : Env) (σ : MTy) : MTy :=
  match Γ with
  | empty       => σ
  | etvar Γ'    => renMTy (weakenMTy Γ' σ) S
  | evar Γ' ρ'  => weakenMTy Γ' σ
  | eimpl Γ' ρ' => weakenMTy Γ' σ
  end.

Lemma CtoFTy_weakenTy : forall Γ ρ,
  CtoFTy (weakenTy Γ ρ) = F.weakenTy (CtoFEnv Γ) (CtoFTy ρ).
Proof.
intro Γ. induction Γ; simpl; intros; trivial.
rewrite <- IHΓ. rewrite CtoFTy_renTy_comm. trivial.
Qed.

Lemma weakenTy_appendEnv : forall Γ1 Γ2 ρ,
  weakenTy (appendEnv Γ1 Γ2) ρ = weakenTy Γ2 (weakenTy Γ1 ρ).
Proof.
intros Γ1 Γ2. induction Γ2; intro ρ'; simpl; auto;
f_equal; auto.
Qed.

Lemma MTy2Ty_weakenTy : forall Γ σ,
  MTy2Ty (weakenMTy Γ σ) = weakenTy Γ (MTy2Ty σ).
Proof.
intro Γ. induction Γ; simpl; trivial. intro σ.
rewrite <- IHΓ. rewrite MTy2Ty_renMTy. trivial.
Qed.

Lemma renTy_weakenTy_S : forall (Γ : Env) (ρ : Ty),
  weakenTy Γ (renTy ρ S) = renTy (weakenTy Γ ρ) S.
Proof.
intro. induction Γ; intro ρ'; simpl; trivial.
f_equal. auto.
Qed.
End TypeWeakening.

Section RenamingsWeakening.
Fixpoint weakenRenTy (Γ : Env) (r : Ren) : Ren :=
  match Γ with
  | empty      => r
  | etvar Γ'   => ren_up (weakenRenTy Γ' r)
  | evar Γ' _  => weakenRenTy Γ' r
  | eimpl Γ' _ => weakenRenTy Γ' r
  end.

Fixpoint weakenRen (Γ : Env) (r : Ren) : Ren :=
  match Γ with
  | empty      => r
  | etvar Γ'   => weakenRen Γ' r
  | evar Γ' _  => ren_up (weakenRen Γ' r)
  | eimpl Γ' _ => ren_up (weakenRen Γ' r)
  end.

Fixpoint weakenRenEnvTy (Γ : Env) : Ren :=
  match Γ with
  | empty      => ren_id
  | etvar Γ'   => ren_comp (weakenRenEnvTy Γ') S
  | evar Γ' _  => weakenRenEnvTy Γ'
  | eimpl Γ' _ => weakenRenEnvTy Γ'
  end.

Fixpoint weakenSubTEnv (Γ : Env) : SubMT :=
  match Γ with
  | empty      => submt_id
  | etvar Γ'   => submt_comp (weakenSubTEnv Γ') (ren_to_submt S)
  | evar Γ' _  => weakenSubTEnv Γ'
  | eimpl Γ' _ => weakenSubTEnv Γ'
  end.

Lemma weakenTy2Ren : forall Γ ρ,
  weakenTy Γ ρ = renTy ρ (weakenRenEnvTy Γ).
Proof.
intro Γ. induction Γ as [|Γ' IH|Γ' ρ' IH|Γ' ρ' IH]; intro ρ; simpl; auto.
+ rewrite renTy_id. trivial.
+ rewrite IH, renTy_comp. f_equal.
Qed.

Lemma weakenTy2SubT : forall Γ ρ,
  weakenTy Γ ρ = subTy ρ (weakenSubTEnv Γ).
Proof.
intro Γ. induction Γ as [|Γ' IH|Γ' ρ' IH|Γ' ρ' IH]; intro ρ; simpl; trivial.
+ rewrite subTy_id. trivial.
+ rewrite renTy_to_subTy, IH, submt_inst_comp. trivial.
Qed.

Lemma weakenRenTy_weakenRenEnvTy : forall (Γ : Env) (n : nat) (r : Ren),
  weakenRenTy Γ r (weakenRenEnvTy Γ n) = weakenRenEnvTy Γ (r n).
Proof.
intro Γ. induction Γ; simpl in *; intros;
unfold ren_id, id, ren_comp; trivial. f_equal. trivial.
Qed.

Lemma ren_comp_weakenRenTy_weakenRenEnvTy : forall Γ,
  ren_comp (weakenRenEnvTy Γ) (weakenRenTy Γ S) =
    ren_comp S (weakenRenEnvTy Γ).
Proof.
intro Γ. induction Γ; simpl; unfold ren_comp, ren_id, id, ren_up;
extensionality i; destruct i; trivial;
try apply weakenRenTy_weakenRenEnvTy; unfold snoc, ren_comp;
f_equal; apply weakenRenTy_weakenRenEnvTy.
Qed.

Lemma weakenRenEnvTy_weakenSubTEnv : forall Γ,
  ren_to_submt (weakenRenEnvTy Γ) = weakenSubTEnv Γ.
Proof.
intro Γ. induction Γ; simpl; trivial.
rewrite <- submt_comp_ren_to_submt, <- IHΓ.
trivial.
Qed.

Lemma weakenRenTy_appendEnv : forall Γ1 Γ2 r,
  weakenRenTy (appendEnv Γ1 Γ2) r = weakenRenTy Γ2 (weakenRenTy Γ1 r).
Proof.
intros Γ1 Γ2. revert Γ1. induction Γ2; simpl; intros; trivial.
f_equal. trivial.
Qed.
End RenamingsWeakening.

Section TypeSubstitutionsWeakening.
Fixpoint weakenSubMT (Γ : Env) (s : SubMT) : SubMT :=
  match Γ with
  | empty      => s
  | etvar Γ'   => submt_up (weakenSubMT Γ' s)
  | evar Γ' _  => weakenSubMT Γ' s
  | eimpl Γ' _ => weakenSubMT Γ' s
  end.

Lemma weakenSubMT_submt_id : forall Γ,
  weakenSubMT Γ submt_id = submt_id.
Proof.
intro Γ. induction Γ; simpl; trivial.
rewrite IHΓ. apply submt_id_up.
Qed.

Lemma CtoFSubT_weakenSubT : forall Γ s,
  CtoFSubT (weakenSubMT Γ s) = F.weakenSubTy (CtoFEnv Γ) (CtoFSubT s).
Proof.
intros. induction Γ; simpl; auto.
rewrite <- IHΓ. rewrite CtoFSubT_submt_up. trivial.
Qed.

Lemma renTy_subTy_weaken : forall (Γ : Env) (ξ : SubMT) (T : Ty),
  subTy (renTy T (weakenRenTy Γ S)) (weakenSubMT Γ (submt_up ξ)) =
  renTy (subTy T (weakenSubMT Γ ξ)) (weakenRenTy Γ S).
Proof.
intros. dbruijn. f_equal. induction Γ; simpl; auto.
- rewrite ren_up_to_submt_up. repeat rewrite submt_comp_submt_up.
  rewrite IHΓ. trivial.
Qed.

Lemma weakenSubMT_weakenRenEnvTy : forall (Γ : Env) (n : nat) (s : SubMT),
  weakenSubMT Γ s (weakenRenEnvTy Γ n) = weakenMTy Γ (s n).
Proof.
intro Γ. induction Γ; simpl in *; intros;
unfold ren_id, id, ren_comp; trivial. f_equal. trivial.
Qed.

Lemma weakenSubT_snoc : forall (Γ : Env) (σ : MTy),
  weakenSubMT Γ (snoc submt_id σ) (weakenRenEnvTy Γ 0) =
  weakenMTy Γ σ.
Proof.
intros. induction Γ; simpl; trivial.
rewrite IHΓ. trivial.
Qed.

Lemma weakenSubMT_appendEnv : forall Γ1 Γ2 r,
  weakenSubMT (appendEnv Γ1 Γ2) r = weakenSubMT Γ2 (weakenSubMT Γ1 r).
Proof.
intros Γ1 Γ2 r. (* revert Γ1. *) induction Γ2; simpl; intros; trivial.
rewrite IHΓ2. trivial.
Qed.

Lemma weakenRenTy_weakenSubMT : forall Γ r,
  ren_to_submt (weakenRenTy Γ r) = weakenSubMT Γ (ren_to_submt r).
Proof.
intro Γ. induction Γ; simpl; intro r; trivial.
rewrite <- IHΓ. rewrite ren_up_to_submt_up. trivial.
Qed.

Lemma submt_comp_weakenSubMT : forall Γ s1 s2,
  submt_comp (weakenSubMT Γ s1) (weakenSubMT Γ s2) =
  weakenSubMT Γ (submt_comp s1 s2).
Proof.
intro Γ. induction Γ; simpl; intros s1 s2; trivial.
rewrite <- IHΓ. dbruijn. trivial.
Qed.
End TypeSubstitutionsWeakening.

Section EnvironmentWeakening.
Fixpoint renEnv (Γ : Env) (r : Ren) : Env :=
  match Γ with
  | empty      => empty
  | etvar Γ'   => etvar (renEnv Γ' r)
  | evar Γ' ρ  => evar (renEnv Γ' r) (renTy ρ (weakenRenTy Γ' r))
  | eimpl Γ' ρ => eimpl (renEnv Γ' r) (renTy ρ (weakenRenTy Γ' r))
  end.

Fixpoint subtEnv (Γ : Env) (s : SubMT) : Env :=
  match Γ with
  | empty     => empty
  | etvar Γ'  => etvar (subtEnv Γ' s)
  | evar Γ' ρ => evar (subtEnv Γ' s) (subTy ρ (weakenSubMT Γ' s))
  | eimpl Γ' ρ => eimpl (subtEnv Γ' s) (subTy ρ (weakenSubMT Γ' s))
  end.

Lemma CtoFEnv_subtEnv : ∀ Γ s,
  CtoFEnv (subtEnv Γ s) = F.subtEnv (CtoFEnv Γ) (CtoFSubT s).
Proof.
intros. induction Γ; simpl; auto; rewrite <- IHΓ; isimpl; dbruijn; f_equal; translation; auto; rewrite CtoFSubT_weakenSubT; auto.
Qed.

Lemma renEnv_appendEnv : forall r Γ1 Γ2,
  renEnv (appendEnv Γ1 Γ2) r =
  appendEnv (renEnv Γ1 r) (renEnv Γ2 (weakenRenTy Γ1 r)).
Proof.
intros r Γ1 Γ2. induction Γ2; simpl; intros; trivial;
rewrite IHΓ2; f_equal; rewrite weakenRenTy_appendEnv;
trivial.
Qed.

Lemma subtEnv_appendEnv : forall s Γ1 Γ2,
  subtEnv (appendEnv Γ1 Γ2) s =
  appendEnv (subtEnv Γ1 s) (subtEnv Γ2 (weakenSubMT Γ1 s)).
Proof.
intros r Γ1 Γ2. induction Γ2; simpl; intros; trivial;
rewrite IHΓ2; f_equal; rewrite weakenSubMT_appendEnv;
trivial.
Qed.

Lemma weakenRenEnvTy_renEnv : forall Γ r,
  weakenRenEnvTy (renEnv Γ r) = weakenRenEnvTy Γ.
Proof.
intro Γ. induction Γ; simpl; intros r; trivial.
f_equal. trivial.
Qed.

Lemma weakenSubMT_renEnv : forall Γ r s,
  weakenSubMT (renEnv Γ r) s = weakenSubMT Γ s.
Proof.
intro Γ. induction Γ; simpl; intros r s; trivial.
f_equal. trivial.
Qed.
Hint Rewrite weakenRenTy_weakenSubMT weakenSubMT_renEnv
  submt_comp_weakenSubMT weakenSubMT_submt_id : infraC.

Lemma subtEnv_renEnv_S_snoc : forall Γ ρ,
  subtEnv (renEnv Γ S) (snoc submt_id ρ) = Γ.
Proof.
intro Γ. induction Γ; simpl; intro; trivial;
f_equal; trivial; dbruijn; auto.
Qed.

Lemma weakenRenTy_ren_id (Γ : Env) :
  weakenRenTy Γ ren_id = ren_id.
Proof.
induction Γ; simpl; auto.
rewrite IHΓ. rewrite ren_id_up. trivial.
Qed.

Lemma renEnv_ren_id (Γ : Env) :
  renEnv Γ ren_id = Γ.
Proof.
induction Γ; simpl; f_equal; auto;
rewrite weakenRenTy_ren_id, renTy_id; trivial.
Qed.

Lemma shift_etvar_weaken : forall r Γ1 Γ2,
  shift_etvar r Γ1 Γ2 -> forall Δ,
  shift_etvar (weakenRenTy Δ r) (appendEnv Γ1 Δ) (appendEnv Γ2 (renEnv Δ r)).
Proof.
intros r Γ1 Γ2 sh Δ. induction Δ; simpl; try constructor; trivial.
Qed.

Lemma shift_eimpl_weaken : forall r Γ1 Γ2,
  shift_eimpl r Γ1 Γ2 -> forall Δ,
  shift_eimpl (weakenRen Δ r) (appendEnv Γ1 Δ) (appendEnv Γ2 Δ).
Proof.
intros r Γ1 Γ2 sh Δ. induction Δ; simpl; try constructor; trivial.
Qed.

Lemma subt_etvar_weaken Γ σ : forall s Γ1 Γ2,
  submt_etvar Γ σ s Γ1 Γ2 -> forall Δ,
  submt_etvar Γ σ (weakenSubMT Δ s) (appendEnv Γ1 Δ) (appendEnv Γ2 (subtEnv Δ s)).
Proof.
intros s Γ1 Γ2 se Δ. revert s Γ1 Γ2 se. induction Δ; 
simpl; intros; try constructor; auto.
Qed.
End EnvironmentWeakening.
End Weakening.
Hint Rewrite weakenRenTy_weakenSubMT weakenSubMT_renEnv
  submt_comp_weakenSubMT weakenSubMT_submt_id : infraC.

Section Lookups.
Inductive lookup_evar : Env -> nat -> Ty -> Prop :=
  | lookup_evar_here {Γ : Env} {ρ : Ty} :
    lookup_evar (evar Γ ρ) 0 ρ
  | lookup_evar_there_evar {Γ : Env} {ρ ρ': Ty} x :
    lookup_evar Γ x ρ -> lookup_evar (evar Γ ρ') (S x) ρ
  | lookup_evar_there_etvar {Γ : Env} {ρ : Ty} x :
    lookup_evar Γ x ρ -> lookup_evar (etvar Γ) x (renTy ρ S)
  | lookup_evar_there_impl {Γ : Env} {ρ ρ': Ty} x :
    lookup_evar Γ x ρ -> lookup_evar (eimpl Γ ρ') (S x) ρ.

Inductive lookup_etvar : Env → nat → Prop :=
  | lookup_etvar_here {Γ: Env} :
      lookup_etvar (etvar Γ) O
  | lookup_etvar_there_evar {Γ: Env} {ρ x} :
      lookup_etvar Γ x →
      lookup_etvar (evar Γ ρ) x
  | lookup_etvar_there_etvar {Γ: Env} {x} :
      lookup_etvar Γ x →
      lookup_etvar (etvar Γ) (S x)
  | lookup_etvar_there_eimpl {Γ : Env} {ρ x} :
    lookup_etvar Γ x -> 
    lookup_etvar (eimpl Γ ρ) x.

Inductive lookup_impl : Env -> nat -> Ty -> Prop :=
  | lookup_impl_here {Γ : Env} {ρ} :
    lookup_impl (eimpl Γ ρ) 0 ρ
  | lookup_impl_there_etvar {Γ : Env} {ρ x} :
    lookup_impl Γ x ρ -> lookup_impl (etvar Γ) x (renTy ρ S)
  | lookup_impl_there_evar {Γ : Env} {ρ ρ' x} : 
    lookup_impl Γ x ρ -> lookup_impl (evar Γ ρ') (S x) ρ
  | lookup_impl_there_eimpl {Γ : Env} {ρ ρ' x} :
    lookup_impl Γ x ρ -> lookup_impl (eimpl Γ ρ') (S x) ρ.

Hint Constructors lookup_evar lookup_etvar lookup_etvar lookup_impl.

Lemma CtoF_lookup_evar (Γ : Env) (ρ : Ty) (x : nat) :
  lookup_evar Γ x ρ -> F.lookup_evar (CtoFEnv Γ) x (CtoFTy ρ).
Proof.
induction 1; simpl; translation; auto.
Qed.

Lemma CtoF_lookup_etvar (Γ : Env) (x : nat) :
  lookup_etvar Γ x -> F.lookup_etvar (CtoFEnv Γ) x.
Proof.
induction 1; simpl; auto.
Qed.

Lemma CtoF_lookup_impl_evar (Γ : Env) (ρ : Ty) (x : nat) :
  lookup_impl Γ x ρ -> F.lookup_evar (CtoFEnv Γ) x (CtoFTy ρ).
Proof.
induction 1; simpl; translation; auto.
Qed.

Lemma shift_etvar_lookup_etvar  :
  forall (r : Ren) (G G' : Env) (sh : shift_etvar r G G') (x : nat),
lookup_etvar G x -> lookup_etvar G' (r x).
Proof.
intros r Γ Γ' sh. induction sh; intros x lk; inversion lk; subst; 
repeat constructor; auto.
Qed.

Lemma shift_etvar_lookup_impl  :
  forall (r : Ren) (Γ Γ' : Env) (sh : shift_etvar r Γ Γ') (x : nat) (ρ : Ty),
lookup_impl Γ x ρ -> lookup_impl Γ' x (renTy ρ r).
Proof.
intros r Γ Γ' sh. induction sh; intros x ρ lk; inversion lk; subst; 
try constructor; auto. dbruijn.
rewrite <- submt_comp_submt_ren.
repeat rewrite <- renTy_to_subTy.
constructor. auto.
Qed.

Lemma shift_evar_lookup_impl :
  forall (r : Ren) (Γ Γ' : Env) (sh : shift_evar r Γ Γ') (x : nat) (ρ : Ty),
  lookup_impl Γ x ρ -> lookup_impl Γ' (r x) ρ.
Proof.
intros r Γ Γ' sh. induction sh; intros x ρ0 lk; inversion lk; subst; 
repeat constructor; auto.
Qed.

Lemma shift_eimpl_lookup_impl  :
  forall (r : Ren) (Γ Γ' : Env) (sh : shift_eimpl r Γ Γ') (x : nat) (ρ : Ty),
  lookup_impl Γ x ρ -> lookup_impl Γ' (r x) ρ.
Proof.
intros r Γ Γ' sh. induction sh; intros x ρ0 lk; auto;
inversion lk; subst; simpl; constructor; auto.
Qed.

Lemma shift_evar_lookup_etvar  :
  forall (r : Ren) (Γ Γ' : Env) (sh : shift_evar r Γ Γ') (x : nat),
  lookup_etvar Γ x -> lookup_etvar Γ' x.
Proof.
intros r Γ Γ' sh. induction sh; intros x lk; auto;
inversion lk; subst; simpl; constructor; auto.
Qed.

Lemma shift_eimpl_lookup_etvar  :
  forall (r : Ren) (Γ Γ' : Env) (sh : shift_eimpl r Γ Γ') (x : nat),
  lookup_etvar Γ x -> lookup_etvar Γ' x.
Proof.
intros r Γ Γ' sh. induction sh; intros x lk; auto;
inversion lk; subst; simpl; constructor; auto.
Qed.

Lemma submt_etvar_lookup_impl (Γ : Env) (σ : MTy) :
  forall (Γ1 Γ2 : Env) (ρ' : Ty) (s : SubMT) (sh : submt_etvar Γ σ s Γ1 Γ2) (x : nat),
  lookup_impl Γ1 x ρ' ->
  lookup_impl Γ2 x (subTy ρ' s).
Proof.
intros Γ1 Γ2 ρ' s sh. revert ρ'. induction sh; intros ρ1 x lk;
inversion lk; subst; dbruijn; auto.
rewrite <- submt_comp_submt_ren, <- renTy_to_subTy.
constructor; auto.
Qed.
End Lookups.
Hint Rewrite CtoF_lookup_evar CtoF_lookup_etvar
  CtoF_lookup_impl_evar : trans_comm.
Hint Resolve CtoF_lookup_impl_evar : CtoF.
Hint Resolve shift_etvar_lookup_etvar shift_etvar_lookup_impl
  shift_evar_lookup_impl shift_eimpl_lookup_impl
  shift_evar_lookup_etvar shift_eimpl_lookup_etvar : shift.
Hint Resolve submt_etvar_lookup_impl : substT.
Hint Constructors lookup_evar lookup_etvar lookup_etvar lookup_impl submt_etvar.
Hint Constructors lookup_evar lookup_etvar lookup_etvar lookup_impl.

Ltac shift_lookup :=
  match goal with
  | [ _ : lookup_etvar ?Γ1 _,
      sh : shift_etvar _ ?Γ1 ?Γ2 |- lookup_etvar ?Γ2 _] =>
      eapply shift_etvar_lookup_etvar
  | [ _ : lookup_etvar ?Γ1 _,
      sh : shift_evar _ ?Γ1 ?Γ2 |- lookup_etvar ?Γ2 _] =>
      eapply shift_evar_lookup_etvar
    | [ _ : lookup_etvar ?Γ1 _,
      sh : shift_eimpl _ ?Γ1 ?Γ2 |- lookup_etvar ?Γ2 _] =>
      eapply shift_eimpl_lookup_etvar
    | [ _ : lookup_impl ?Γ1 _,
      sh : shift_eimpl _ ?Γ1 ?Γ2 |- lookup_impl ?Γ2 _] =>
      eapply shift_eimpl_lookup_impl
    | [ _ : lookup_impl ?Γ1 _ _,
      sh : shift_etvar _ ?Γ1 ?Γ2 |- lookup_impl ?Γ2 _ _] =>
      eapply shift_etvar_lookup_impl
    | [ _ : lookup_impl ?Γ1 _ _,
      sh : shift_evar _ ?Γ1 ?Γ2 |- lookup_impl ?Γ2 _ _] =>
      eapply shift_evar_lookup_impl
  end; eauto.

Inductive wfTy (Γ : Env) : Ty -> Prop :=
  | wf_tvar (x : nat) : lookup_etvar Γ x 
    -> wfTy Γ (tvar x)
  | wf_tarr (ρ1 ρ2 : Ty): wfTy Γ ρ1 -> wfTy Γ ρ2
    -> wfTy Γ (tarr ρ1 ρ2)
  | wf_tall (ρ : Ty) : wfTy (etvar Γ) ρ
    -> wfTy Γ (tall ρ)
  | wf_tiarr (ρ1 ρ2 : Ty): wfTy Γ ρ1 -> wfTy Γ ρ2
    -> wfTy Γ (tiarr ρ1 ρ2).

Inductive amb_res (Γ : Env) : Ty -> F.Tm -> Prop :=
  | AR_Ivar x ρ : lookup_impl Γ x ρ
    -> amb_res Γ ρ (F.var x)
  | AR_Tabs E ρ : amb_res (etvar Γ) ρ E
    -> amb_res Γ (tall ρ) (F.tabs E)
  | AR_Iabs E ρ1 ρ2 : amb_res (eimpl Γ ρ1) ρ2 E
    -> amb_res Γ (tiarr ρ1 ρ2) (F.abs (CtoFTy ρ1) E)
  | AR_Tapp E ρ σ : amb_res Γ (tall ρ) E (* -> wfTy Γ ρ' *)
    -> amb_res Γ (subTy ρ (snoc submt_id σ)) (F.tapp E (CtoFTy (MTy2Ty σ)))
  | AR_Iapp E1 E2 ρ1 ρ2 : amb_res Γ (tiarr ρ1 ρ2) E2 -> amb_res Γ ρ1 E1
    -> amb_res Γ ρ2 (F.app E2 E1).

(* Lemma 5.1 *)
Lemma amb_res_TypePreservation (Γ : Env) (ρ : Ty) (E : F.Tm) :
  amb_res Γ ρ E -> F.Typing (CtoFEnv Γ) E (CtoFTy ρ).
Proof.
induction 1; simpl in *; translation;
econstructor; eauto using CtoF_lookup_impl_evar.
Qed.

Lemma shift_etvar_amb_res (Γ : Env) (ρ : Ty) (E : F.Tm) (ar : amb_res Γ ρ E) :
  forall (r : Ren) (Γ' : Env) (sh : shift_etvar r Γ Γ'),
  amb_res Γ' (renTy ρ r) (F.renTm E r F.ren_id).
Proof.
induction ar; intros r Γ' sh; (* dbruijn. *) simpl in *.
- constructor. unfold F.ren_id, id. shift_lookup.
- constructor. eauto.
- rewrite <- CtoFTy_renTy_comm. constructor.
  rewrite F.ren_id_up. eauto.
- rewrite <- CtoFTy_renTy_comm.
  specialize (IHar _ _ sh).
  apply (AR_Tapp _ _ _ (renMTy σ r)) in IHar. dbruijn. trivial.
- econstructor; eauto.
Qed.

Lemma shift_evar_amb_res (Γ : Env) (ρ : Ty) (E : F.Tm) (ar : amb_res Γ ρ E) :
  forall (r : Ren) (Γ' : Env) (sh : shift_evar r Γ Γ'),
  amb_res Γ' ρ (F.renTm E (@id nat) r).
Proof.
induction ar; intros; simpl.
- constructor.
  apply (shift_evar_lookup_impl) with Γ; trivial.
- constructor. change (id (A:=nat)) with (ren_id) in *.
  rewrite F.ren_id_up. apply IHar. constructor. trivial.
- rewrite F.renTy_id. constructor. apply IHar.
  constructor. trivial.
- rewrite F.renTy_id. constructor. apply IHar. trivial.
- specialize (IHar1 _ _ sh). specialize (IHar2 _ _ sh).
  apply (AR_Iapp _ _ _ _ _ IHar1 IHar2).
Qed.

Lemma shift_eimpl_amb_res (Γ : Env) (ρ : Ty) (E : F.Tm) (ar : amb_res Γ ρ E) :
  forall (r : Ren) (Γ' : Env) (sh : shift_eimpl r Γ Γ'),
  amb_res Γ' ρ (F.renTm E (@id nat) r).
Proof.
induction ar; intros; simpl.
- constructor. apply (shift_eimpl_lookup_impl) with Γ; trivial.
- constructor. change (id (A:=nat)) with (ren_id) in *.
  rewrite F.ren_id_up. apply IHar. constructor. trivial.
- rewrite F.renTy_id. constructor. apply IHar.
  constructor. trivial.
- rewrite F.renTy_id. constructor. apply IHar. trivial.
- specialize (IHar1 _ _ sh). specialize (IHar2 _ _ sh).
  apply (AR_Iapp _ _ _ _ _ IHar1 IHar2).
Qed.

Lemma submt_etvar_amb_res (Γ : Env) (σ : MTy) (* (wf : wfTy Γ ρ) *) :
  forall (Γ1 Γ2 : Env) (s : SubMT) (sh : submt_etvar Γ σ s Γ1 Γ2)
         (ρ' : Ty) (E : F.Tm),
    amb_res Γ1 ρ' E ->
    amb_res Γ2 (subTy ρ' s) (F.subTm E (CtoFSubT s) F.sub_id).
Proof.
intros Γ1 Γ2 s sh ρ' E ar. revert Γ2 s sh.
induction ar; simpl in *; intros Γ2 s sh.
- constructor. eapply submt_etvar_lookup_impl; eauto.
- constructor. translation. rewrite F.sub_id_upT.
  apply IHar. constructor. auto.
- rewrite <- CtoFTy_subTy. constructor.
  rewrite F.sub_id_up.
  apply IHar. constructor. trivial.
- rewrite <- CtoFTy_subTy, <- MTy2Ty_subMTy.
  rewrite submt_inst_comp, submt_comp_snoc.
  rewrite <- submt_comp_up_id, <- submt_inst_comp, submt_comp_id_left.
  constructor. auto.
- econstructor; eauto.
Qed.

(* Cochis terms *)
Inductive Tm : Set :=
  | var (x : nat)
  | abs (ρ : Ty) (t : Tm)
  | app (t1 t2 : Tm)
  | tabs (t : Tm)
  | tapp (t : Tm) (σ : MTy)
  | ivar (ρ : Ty)
  | iabs (ρ : Ty) (t : Tm)
  | iapp (t1 t2 : Tm).

(* Term renaming *)
Fixpoint renTm (ρ : Tm) (ξΤ : nat -> nat) (ξ : nat -> nat) : Tm :=
  match ρ with
  | var x      => var (ξ x)
  | abs ρ t    => abs (renTy ρ ξΤ) (renTm t ξΤ (ren_up ξ))
  | app t1 t2  => app (renTm t1 ξΤ ξ) (renTm t2 ξΤ ξ)
  | tabs t     => tabs (renTm t (ren_up ξΤ) ξ)
  | tapp t σ   => tapp (renTm t ξΤ ξ) (renMTy σ ξΤ)
  | ivar ρ     => ivar (renTy ρ ξΤ)
  | iabs ρ t   => iabs (renTy ρ ξΤ) (renTm t ξΤ (ren_up ξ))
  | iapp t1 t2 => iapp (renTm t1 ξΤ ξ) (renTm t2 ξΤ ξ)
  end.

Lemma renTm_comp t : forall ξT₁ ξt₁ ξT₂ ξt₂,
  renTm (renTm t ξT₁ ξt₁) ξT₂ ξt₂ = renTm t (ren_comp ξT₁ ξT₂) (ren_comp ξt₁ ξt₂).
Proof.
induction t; simpl; intros; auto; repeat rewrite renTy_comp; auto; f_equal; try (
match goal with
 [H : context[renTm _ _ _ = renTm _ _ _] |- _ ] => rewrite H
end; f_equal); dbruijn; trivial.
Qed.
Hint Rewrite renTm_comp : infraC.

Lemma shift_shiftT_comm : forall (t : Tm),
  renTm (renTm t S ren_id) ren_id S = renTm (renTm t ren_id S) S ren_id.
Proof.
intro t; unfold shiftT, shift. destruct t; simpl; trivial;
f_equal; change (id (A:=nat)) with ren_id; autorewrite with infraC; trivial.
Qed.

(* Weakening with an environment to be appended to the initial one *)

Fixpoint weakenIndexTy (Γ : Env) (x : nat) : nat :=
  match Γ with
  | empty      => x
  | etvar Γ'   => S (weakenIndexTy Γ' x)
  | evar Γ' _  => weakenIndexTy Γ' x
  | eimpl Γ' _ => weakenIndexTy Γ' x
  end.

Fixpoint weakenIndexTm (Γ : Env) (x : nat) : nat :=
  match Γ with
  | empty      => x
  | etvar Γ'   => weakenIndexTm Γ' x
  | evar Γ' _  => S (weakenIndexTm Γ' x)
  | eimpl Γ' _ => S (weakenIndexTm Γ' x)
  end.

Fixpoint weakenTm (Γ : Env) (t : Tm) : Tm :=
  match Γ with
  | empty      => t
  | etvar Γ'   => weakenTm Γ' (renTm t S (@id nat))
  | evar Γ' _  => weakenTm Γ' (renTm t (@id nat) S)
  | eimpl Γ' _ => weakenTm Γ' (renTm t (@id nat) S)
  end.

Lemma weakenIndexTm_appendEnv : forall Γ1 Γ2 x,
  weakenIndexTm (appendEnv Γ1 Γ2) x = weakenIndexTm Γ2 (weakenIndexTm Γ1 x).
Proof.
intros Γ1 Γ2. induction Γ2; intro x; simpl; auto.
Qed.

(* Term substitution *)

Definition Sub := nat -> Tm.
Definition sub_id : Sub := fun i => var i.
Definition sub_upT (ζ: Sub) : Sub :=
  fun i => renTm (ζ i) S ren_id.
Definition sub_upt (ζ: Sub) : Sub :=
  fun i => match i with
             | O   => var O
             | S i => renTm (ζ i) ren_id S
           end.

Lemma sub_id_up : sub_upt sub_id = sub_id.
Proof.
  extensionality i; destruct i; reflexivity.
Qed.

Lemma sub_id_upT : sub_upT sub_id = sub_id.
Proof.
  extensionality i; destruct i; reflexivity.
Qed.
Hint Rewrite sub_id_up sub_id_upT : infraC.

Definition ren_to_sub (ξ: Ren) : Sub := fun i => var (ξ i).

Fixpoint subTm (e : Tm) (st : SubMT) (s : Sub) : Tm :=
  match e with
  | var x      => s x
  | abs ρ e'   => abs (subTy ρ st) (subTm e' st (sub_upt s))
  | app e1 e2  => app (subTm e1 st s) (subTm e2 st s)
  | tabs e'    => tabs (subTm e' (submt_up st) (sub_upT s))
  | tapp e' σ  => tapp (subTm e' st s) (subMTy σ st)
  | ivar ρ     => ivar (subTy ρ st)
  | iabs ρ e'  => iabs (subTy ρ st) (subTm e' st (sub_upt s))
  | iapp e1 e2 => iapp (subTm e1 st s) (subTm e2 st s)
  end.

Definition sub_comp (st1 : SubMT) (s1 : Sub) (st2 : SubMT) (s2 : Sub) :=
  fun i => subTm (s1 i) st2 s2.

Lemma sub_comp_id_left ξ : 
  sub_comp submt_id sub_id submt_id ξ = ξ.
Proof.
  extensionality i; destruct i; reflexivity.
Qed.

Lemma sub_eta (ζT: SubMT) (ζt: Sub) :
  snoc (sub_comp submt_id (ren_to_sub S) ζT ζt) (ζt 0) = ζt.
Proof. extensionality i; destruct i; reflexivity. Qed.
Hint Rewrite sub_eta sub_comp_id_left : infraC.

Lemma subTm_id t : subTm t submt_id sub_id = t.
Proof.
induction t; simpl; f_equal; auto; dbruijn; trivial.
Qed.
Hint Rewrite subTm_id : infraC.

Lemma ren_id_to_sub : ren_to_sub (ren_id) = sub_id.
Proof. reflexivity. Qed.
Hint Rewrite ren_id_to_sub : infraC.

Lemma sub_reflection (ζT:SubT) (ζt: Sub) s :
  sub_comp submt_id (ren_to_sub S) submt_id (snoc ζt s) = ζt.
Proof.
  extensionality i; destruct i; reflexivity.
Qed.

Lemma sub_comp_snoc ζt₁ ζt₂ s :
  sub_comp submt_id (snoc ζt₁ s) submt_id ζt₂ = snoc (sub_comp submt_id ζt₁ submt_id ζt₂) (subTm s submt_id ζt₂).
Proof. extensionality i; destruct i; reflexivity. Qed.
Hint Rewrite sub_comp_snoc.

Lemma ren_up_to_sub_up ξ : ren_to_sub (ren_up ξ) = sub_upt (ren_to_sub ξ).
Proof.
  extensionality i; destruct i; reflexivity.
Qed.
Hint Rewrite ren_up_to_sub_up : infraC.

Lemma sub_comp_id_right ζ : sub_comp submt_id ζ submt_id sub_id = ζ.
Proof.
  extensionality i; unfold sub_comp.
  rewrite subTm_id; reflexivity.
Qed.

Lemma renTm_to_subTm t : forall ξT ξt,
  renTm t ξT ξt = subTm t (ren_to_submt ξT) (ren_to_sub ξt).
Proof.
induction t; simpl; intros; auto; dbruijn; f_equal;
match goal with
| [H : context[renTm _ _ _ = subTm _ _ _]
    |- _ ] => rewrite H
  end; dbruijn; trivial.
Qed.
Hint Rewrite renTm_to_subTm : infraC.

Lemma sub_up_def (ζ: Sub) : 
sub_upt ζ = snoc (sub_comp submt_id ζ submt_id (ren_to_sub S)) (var 0).
Proof.
extensionality i; destruct i; simpl; dbruijn; reflexivity.
Qed.

Lemma sub_comp_ren_sub_upT ζT₁ ζT₂ ξ ζ :
  sub_comp (submt_up ζT₁) (sub_upT (ren_to_sub ξ)) (submt_up ζT₂) (sub_upT ζ)
  = sub_upT (sub_comp ζT₁ (ren_to_sub ξ) ζT₂ ζ).
Proof. extensionality i; destruct i; reflexivity. Qed.

Lemma sub_comp_ren_sub_up ζT₁ ζT₂ ξ ζ :
  sub_comp (submt_up ζT₁) (sub_upt (ren_to_sub ξ)) (submt_up ζT₂) (sub_upt ζ)
  = sub_upt (sub_comp ζT₁ (ren_to_sub ξ) ζT₂ ζ).
Proof. extensionality i; destruct i; reflexivity. Qed.

Lemma sub_upT_ren_to_sub ξ : sub_upT (ren_to_sub ξ) = ren_to_sub ξ.
Proof. reflexivity. Qed.

Lemma sub_comp_ren_sub s : forall ζT₁ ζT₂ ξ ζ,
  subTm (subTm s ζT₁ (ren_to_sub ξ)) ζT₂ ζ =
  subTm s (submt_comp ζT₁ ζT₂) (sub_comp ζT₁ (ren_to_sub ξ) submt_id ζ).
Proof.
induction s; simpl; intros; auto; try rewrite submt_inst_comp; f_equal; auto; try (
match goal with
| [ |- context[sub_upt (ren_to_sub _)]] => rewrite <- ren_up_to_sub_up
| [ |- context[sub_upT (ren_to_sub _)]] => rewrite sub_upT_ren_to_sub
end; rewrite IHs; f_equal); dbruijn; f_equal;
rewrite <- sub_comp_ren_sub_up; dbruijn; reflexivity.
Qed.
Hint Rewrite sub_comp_ren_sub : infraC.

Lemma sub_comp_sub_ren_up ζT₁ ζT₂ ξ ζ :
  sub_comp (submt_up ζT₁) (sub_upt ζ) ζT₂ (sub_upt (ren_to_sub ξ)) =
  sub_upt (sub_comp ζT₁ ζ ζT₂ (ren_to_sub ξ)).
Proof.
unfold sub_comp, sub_upT. extensionality i; destruct i; dbruijn; f_equal.
Qed.

Lemma sub_comp_sub_ren_upT ζT₁ ζT₂ ξ ζ :
  sub_comp (submt_up ζT₁) (sub_upT ζ) (submt_up ζT₂) (sub_upT (ren_to_sub ξ)) =
  sub_upT (sub_comp ζT₁ ζ ζT₂ (ren_to_sub ξ)).
Proof.
extensionality i.
unfold sub_upT, sub_comp.
repeat rewrite renTm_to_subTm.
dbruijn. f_equal.
Qed.

Lemma sub_comp_sub_ren s : forall ζT₁ ζT₂ ξ ζ,
  subTm (subTm s ζT₁ ζ) ζT₂ (ren_to_sub ξ) =
  subTm s (submt_comp ζT₁ ζT₂) (sub_comp ζT₁ ζ ζT₂ (ren_to_sub ξ)).
Proof.
induction s; intros; simpl; auto; try rewrite submt_inst_comp; f_equal; auto.
- rewrite <- sub_comp_sub_ren_up.
  rewrite <- ren_up_to_sub_up, IHs.
  reflexivity.
- rewrite <- sub_comp_sub_ren_upT.
  rewrite sub_upT_ren_to_sub.
  rewrite IHs, submt_comp_submt_up.
  reflexivity.
- dbruijn. trivial.
- rewrite <- sub_comp_sub_ren_up.
  rewrite <- ren_up_to_sub_up, IHs.
  reflexivity.
Qed.
Hint Rewrite sub_comp_sub_ren : infraC.

Lemma submt_comp_S ζ :
  submt_comp (ren_to_submt S) (submt_up ζ) = submt_comp ζ (ren_to_submt S).
Proof.
extensionality i.
unfold subt_comp. dbruijn. reflexivity.
Qed.
Hint Rewrite submt_comp_S : infraC.

Lemma sub_comp_sub_up ζT₁ ζT₂ ζt₁ ζt₂ :
  sub_comp (submt_up ζT₁) (sub_upt ζt₁) ζT₂ (sub_upt ζt₂) =
  sub_upt (sub_comp ζT₁ ζt₁ ζT₂ ζt₂).
Proof.
extensionality i; destruct i; simpl; dbruijn;
unfold sub_upt, submt_comp, sub_comp; dbruijn;
f_equal. clear. extensionality i. unfold sub_comp.
dbruijn. reflexivity.
Qed.
Hint Rewrite sub_comp_sub_up : infraC.

Lemma sub_comp_sub_upT ζT₁ ζT₂ ζt₁ ζt₂ :
  sub_comp (submt_up ζT₁) (sub_upT ζt₁) (submt_up ζT₂) (sub_upT ζt₂) =
  sub_upT (sub_comp ζT₁ ζt₁ ζT₂ ζt₂).
Proof.
extensionality i.
unfold sub_upT, sub_comp.
repeat rewrite renTm_to_subTm.
dbruijn. f_equal. clear.
extensionality i.
unfold submt_comp, sub_comp. dbruijn.
reflexivity.
Qed.
Hint Rewrite sub_comp_sub_upT : infraC.

Lemma sub_comp_sub t : forall ζT₁ ζT₂ ζt₁ ζt₂,
  subTm (subTm t ζT₁ ζt₁) ζT₂ ζt₂ =
  subTm t (submt_comp ζT₁ ζT₂) (sub_comp ζT₁ ζt₁ ζT₂ ζt₂).
Proof.
induction t; simpl; intros; auto; try rewrite submt_inst_comp; f_equal; auto;
try (rewrite IHt; f_equal; dbruijn); try (rewrite <- sub_comp_sub_up; reflexivity); 
auto. dbruijn. trivial.
Qed.
Hint Rewrite sub_comp_sub : infraC.

Inductive sub_evar (Γ : Env) (e : Tm) (ρ : Ty) (E : F.Tm) : Sub -> F.Sub -> Env -> Env -> Prop :=
  | sub_evar_here : sub_evar Γ e ρ E (snoc sub_id e) (F.snoc F.sub_id E) (evar Γ ρ) Γ
  | sub_evar_there_etvar (Γ1 Γ2 : Env) (s : Sub) (ξ : F.Sub):
      sub_evar Γ e ρ E s ξ Γ1 Γ2 ->
      sub_evar Γ e ρ E (sub_upT s) (F.sub_upT ξ) (etvar Γ1) (etvar Γ2)
  | sub_evar_there_evar (Γ1 Γ2 : Env) (ρ' : Ty) (s : Sub) (ξ : F.Sub) :
      sub_evar Γ e ρ E s ξ Γ1 Γ2 ->
      sub_evar Γ e ρ E (sub_upt s) (F.sub_upt ξ) (evar Γ1 ρ') (evar Γ2 ρ')
  | sub_evar_there_eimpl (Γ1 Γ2 : Env) (ρ' : Ty) (s : Sub) (ξ : F.Sub) :
      sub_evar Γ e ρ E s ξ Γ1 Γ2 ->
      sub_evar Γ e ρ E (sub_upt s) (F.sub_upt ξ) (eimpl Γ1 ρ') (eimpl Γ2 ρ').

Inductive sub_eimpl (Γ : Env) (ρ : Ty) (E : F.Tm) : F.Sub -> Env -> Env -> Prop :=
  | sub_eimpl_here : sub_eimpl Γ ρ E (F.snoc F.sub_id E) (eimpl Γ ρ) Γ
  | sin_eimpl_there_etvar s Γ1 Γ2 : 
      sub_eimpl Γ ρ E s Γ1 Γ2 ->
      sub_eimpl Γ ρ E (F.sub_upT s) (etvar Γ1) (etvar Γ2)
  | sin_eimpl_there_evar s Γ1 Γ2 ρ' : 
      sub_eimpl Γ ρ E s Γ1 Γ2 ->
      sub_eimpl Γ ρ E (F.sub_upt s) (evar Γ1 ρ') (evar Γ2 ρ')
  | sin_eimpl_there_eimpl s Γ1 Γ2 ρ' : 
      sub_eimpl Γ ρ E s Γ1 Γ2 ->
      sub_eimpl Γ ρ E (F.sub_upt s) (eimpl Γ1 ρ') (eimpl Γ2 ρ').

Fixpoint weakenSubTm (Γ : Env) (s : Sub) : Sub :=
  match Γ with
  | empty      => s
  | etvar Γ'   => sub_upT (weakenSubTm Γ' s)
  | evar Γ' _  => sub_upt (weakenSubTm Γ' s)
  | eimpl Γ' _ => sub_upt (weakenSubTm Γ' s)
  end.

Lemma sub_evar_weaken (Γ : Env) (e : Tm) (ρ : Ty) (E : F.Tm) :
  forall (Γ' Γ1 Γ2 : Env) (s : Sub) (ξ : F.Sub),
  sub_evar Γ e ρ E s ξ Γ1 Γ2 ->
  sub_evar Γ e ρ E (weakenSubTm Γ' s) (F.weakenSubTm (CtoFEnv Γ') ξ) (appendEnv Γ1 Γ') (appendEnv Γ2 Γ').
Proof.
intros Γ' Γ1 Γ2 s ξ se. induction Γ'; simpl; try constructor; auto.
Qed.

Lemma sub_evar_lookup_impl (Γ : Env) (e' : Tm) (ρ' : Ty) (E' : F.Tm) :
  amb_res Γ ρ' E' ->
  forall (Γ1 Γ2 : Env) (s : Sub) (ξ : F.Sub) (ρ : Ty) (x : nat) 
         (sh : sub_evar Γ e' ρ' E' s ξ Γ1 Γ2),
  lookup_impl Γ1 x ρ ->
  amb_res Γ2 ρ (ξ x).
Proof.
intros ar Γ1 Γ2 s ξ ρ x sb. revert ρ x.
induction sb; intros ρ x lk; inversion lk; subst; simpl.
- unfold F.sub_id. constructor. trivial.
- eapply shift_etvar_amb_res; auto.
- eapply shift_evar_amb_res; auto.
- constructor. trivial.
- eapply shift_eimpl_amb_res; auto.
Qed.

Lemma sub_eimpl_lookup_impl (Γ : Env) (ρ : Ty) (E : F.Tm) (ar : amb_res Γ ρ E) :
  forall (Γ1 Γ2 : Env) (s : F.Sub),
  sub_eimpl Γ ρ E s Γ1 Γ2 -> forall (x : nat) (ρ' : Ty),
  lookup_impl Γ1 x ρ' -> amb_res Γ2 ρ' (s x).
Proof.
intros Γ1 Γ2 s se. induction se; intros x ρ0 lk; inversion lk; subst; unfold F.sub_id; isimpl; try constructor; trivial.
- eapply shift_etvar_amb_res; eauto.
- rewrite <- ren_id_to_subt, <- F.renTm_to_subTm.
  eapply shift_evar_amb_res; eauto.
- rewrite <- ren_id_to_subt, <- F.renTm_to_subTm.
  eapply shift_eimpl_amb_res; eauto.
Qed.

Lemma sub_evar_lookup_etvar (Γ : Env) (e' : Tm) (ρ' : Ty) (E' : F.Tm) :
  forall (Γ1 Γ2 : Env) (s : Sub) (ξ : F.Sub) (x : nat) 
         (sh : sub_evar Γ e' ρ' E' s ξ Γ1 Γ2),
  lookup_etvar Γ1 x -> lookup_etvar Γ2 x.
Proof.
intros Γ1 Γ2 s ξ x sb lk. revert x lk.
induction sb; intros x lk; inversion lk; subst; auto.
Qed.

Lemma sub_eimpl_lookup_etvar (Γ : Env) (ρ' : Ty) (E' : F.Tm) :
  forall (Γ1 Γ2 : Env) (s : F.Sub) (x : nat) 
         (sh : sub_eimpl Γ ρ' E' s Γ1 Γ2),
  lookup_etvar Γ1 x -> lookup_etvar Γ2 x.
Proof.
intros Γ1 Γ2 s x sb lk. revert x lk.
induction sb; intros x lk; inversion lk; subst; auto.
Qed.

(* Lemma sub_evar_wfTy (Γ : Env) (e' : Tm) (ρ' : Ty) (E' : F.Tm) :
  forall (Γ1 Γ2 : Env) (ρ : Ty) (s : Sub) (ξ : F.Sub)
         (sh : sub_evar Γ e' ρ' E' s ξ Γ1 Γ2),
    wfTy Γ1 ρ -> wfTy Γ2 ρ.
Proof.
intros Γ1 Γ2 ρ s ξ sb wf1. revert s ξ Γ2 sb.
induction wf1; constructor; eauto.
- apply (sub_evar_lookup_etvar Γ e' ρ' E' Γ0 Γ2 s ξ); trivial.
- specialize (IHwf1 (sub_upT s) (F.sub_upT ξ) (etvar Γ2)).
  apply IHwf1. constructor. trivial.
Qed.

Lemma sub_eimpl_wfTy (Γ : Env) (ρ' : Ty) (E' : F.Tm) :
  forall (Γ1 Γ2 : Env) (ρ : Ty) (s : F.Sub)
         (sh : sub_eimpl Γ ρ' E' s Γ1 Γ2),
    wfTy Γ1 ρ -> wfTy Γ2 ρ.
Proof.
intros Γ1 Γ2 ρ s sb wf1. revert s Γ2 sb.
induction wf1; constructor; eauto.
- eapply (sub_eimpl_lookup_etvar); eauto.
- eapply IHwf1. econstructor. eauto.
Qed. *)

Lemma sub_evar_amb_res (Γ : Env) (e' : Tm) (ρ' : Ty) (E' : F.Tm) :
  amb_res Γ ρ' E' ->
  forall (Γ1 Γ2 : Env) (ρ : Ty) (E1 : F.Tm) (s : Sub) (ξ : F.Sub)
         (sh : sub_evar Γ e' ρ' E' s ξ Γ1 Γ2),
    amb_res Γ1 ρ E1 ->
    amb_res Γ2 ρ (F.subTm E1 F.subt_id ξ).
Proof.
intros ar Γ1 Γ2 ρ E1 s ξ sh ar1. revert Γ2 s ξ sh.
induction ar1; simpl; intros Γ2 s ξ sh.
- apply sub_evar_lookup_impl with Γ e' ρ' E' Γ0 s; trivial.
- constructor. rewrite F.subt_id_up.
  apply IHar1 with (sub_upT s). constructor. trivial.
- rewrite F.subTy_id. constructor. apply IHar1 with (sub_upt s).
  constructor. trivial.
- rewrite F.subTy_id. constructor. apply IHar1 with s. trivial.
(*   + eapply sub_evar_wfTy. refine sh. trivial. *)
- econstructor; eauto.
Qed.

Lemma sub_eimpl_amb_res (Γ : Env) (ρ' : Ty) (E' : F.Tm) :
  amb_res Γ ρ' E' ->
  forall (Γ1 Γ2 : Env) (ρ : Ty) (E1 : F.Tm) (s : F.Sub)
         (sh : sub_eimpl Γ ρ' E' s Γ1 Γ2),
    amb_res Γ1 ρ E1 ->
    amb_res Γ2 ρ (F.subTm E1 F.subt_id s).
Proof.
intros ar Γ1 Γ2 ρ E1 s sh ar1. revert Γ2 s sh.
induction ar1; simpl; intros Γ2 s sh.
- eapply sub_eimpl_lookup_impl; eauto.
- constructor. rewrite F.subt_id_up.
  apply IHar1. constructor. trivial.
- rewrite F.subTy_id. constructor. apply IHar1.
  constructor. trivial.
- rewrite F.subTy_id. constructor.
  apply IHar1. trivial.
(*   + eapply sub_eimpl_wfTy; eauto. *)
- econstructor; eauto.
Qed.

(* To be defined... *)
Axiom unamb : Ty -> Prop.

Inductive Typing (Γ : Env) : Tm -> Ty -> F.Tm -> Prop :=
  | TyVar (x : nat) (ρ : Ty) :
    lookup_evar Γ x ρ ->
    Typing Γ (var x) ρ (F.var x)
  | TyAbs (x : nat) (ρ1 ρ2 : Ty) (e : Tm) (E : F.Tm) :
    Typing (evar Γ ρ1) e ρ2 E -> (* wfTy Γ ρ1 -> *)
    Typing Γ (abs ρ1 e) (tarr ρ1 ρ2) (F.abs (CtoFTy ρ1) E)
  | TyApp (e1 e2 : Tm) (ρ1 ρ2 : Ty) (E1 E2 : F.Tm) :
    Typing Γ e1 (tarr ρ1 ρ2) E1 -> Typing Γ e2 ρ1 E2->
    Typing Γ (app e1 e2) ρ2 (F.app E1 E2)
  | TyTabs (e : Tm) (ρ : Ty) (E : F.Tm) :
    Typing (etvar Γ) e ρ E -> 
    Typing Γ (tabs e) (tall ρ) (F.tabs E)
  | TyTapp (e : Tm) (ρ2 : Ty) (σ : MTy) (E : F.Tm) :
    Typing Γ e (tall ρ2) E -> (* wfTy Γ ρ1 -> *)
    Typing Γ (tapp e σ) (subTy ρ2 (snoc submt_id σ)) (F.tapp E (CtoFTy (MTy2Ty σ)))
  | TyIabs (e : Tm) (ρ1 ρ2 : Ty) (E : F.Tm) :
    Typing (eimpl Γ ρ1) e ρ2 E (* -> wfTy Γ ρ1 *) -> unamb ρ1 ->
    Typing Γ (iabs ρ1 e) (tiarr ρ1 ρ2) (F.abs (CtoFTy ρ1) E)
  | TyIapp (e1 e2 : Tm) (ρ1 ρ2 : Ty) (Ε1 Ε2 : F.Tm) :
    Typing Γ e1 (tiarr ρ2 ρ1) Ε1 -> Typing Γ e2 ρ2 Ε2 ->
    Typing Γ (iapp e1 e2) ρ1 (F.app Ε1 Ε2)
  | TyQuer (E : F.Tm) (ρ : Ty) : amb_res Γ ρ E (* -> wfTy Γ ρ *) -> unamb ρ ->
    Typing Γ (ivar ρ) ρ E.

(* Theorem 5.1 *)
Lemma Typing_TypePreservation (Γ : Env) (e : Tm) (ρ : Ty) (E : F.Tm):
  Typing Γ e ρ E -> F.Typing (CtoFEnv Γ) E (CtoFTy ρ).
Proof.
induction 1; simpl in *; try constructor; auto.
* apply CtoF_lookup_evar. trivial.
* eapply F.T_App; eauto.
* translation. constructor. trivial.
* econstructor; eauto.
* pose proof amb_res_TypePreservation Γ ρ E. auto.
Qed.

Lemma weaken_lookup_impl : forall (Γ1 Γ2 : Env) (x : nat) (ρ : Ty), 
  lookup_impl Γ1 x ρ ->
  lookup_impl (appendEnv Γ1 Γ2) (weakenIndexTm Γ2 x) (weakenTy Γ2 ρ).
Proof.
intros. induction Γ2; simpl; try rewrite renTy_weakenTy_comm;
try constructor; trivial.
Qed.

Lemma weaken_lookup_evar : forall (Γ1 Γ2 : Env) (x : nat) (T : Ty),
  lookup_evar Γ1 x T ->
  lookup_evar (appendEnv Γ1 Γ2) (weakenIndexTm Γ2 x) (weakenTy Γ2 T).
Proof.
intros. induction Γ2; simpl; try rewrite renTy_weakenTy_comm;
try constructor; trivial.
Qed.

Lemma CtoFweakenIndexTm : forall (Γ : Env) (x : nat),
  weakenIndexTm Γ x = F.weakenIndexTm (CtoFEnv Γ) x.
Proof.
intros. induction Γ; simpl; f_equal; trivial.
Qed.

Lemma weakenTm_shiftT : forall (Γ Γ' : Env) (t : Tm),
  weakenTm Γ' (renTm t S ren_id) = renTm (weakenTm Γ' t) S ren_id.
Proof.
intros. revert t. induction Γ'; intro t; simpl.
- trivial.
- rewrite IHΓ'. trivial.
- rewrite shift_shiftT_comm. rewrite IHΓ'. trivial.
- rewrite shift_shiftT_comm. rewrite IHΓ'. trivial.
Qed.

Lemma sub_eimpl_weaken (Γ : Env) (ρ : Ty) (E : F.Tm ) : forall s Γ1 Γ2,
  sub_eimpl Γ ρ E s Γ1 Γ2 -> forall Γ',
  sub_eimpl Γ ρ E (F.weakenSubTm (CtoFEnv Γ') s) (appendEnv Γ1 Γ') (appendEnv Γ2 Γ').
Proof.
intros s Γ1 Γ2 se Γ'. induction Γ'; simpl; try constructor; auto.
Qed.

Lemma amb_res_weaken : forall (Γ1 Γ2 : Env) (ρ : Ty) (E : F.Tm),
  amb_res Γ1 ρ E ->
  amb_res (appendEnv Γ1 Γ2) (weakenTy Γ2 ρ) (F.weakenTm (CtoFEnv Γ2) E).
Proof.
intros Γ1 Γ2 ρ E Har. induction Γ2; simpl.
- trivial.
- unfold shiftT.
  apply (shift_etvar_amb_res _ _ _ IHΓ2 S (etvar (appendEnv Γ1 Γ2))).
  constructor.
- unfold shift.
  apply (shift_evar_amb_res _ _ _ IHΓ2 S (evar (appendEnv Γ1 Γ2) ρ0)).
  constructor.
- unfold shift.
  apply (shift_eimpl_amb_res _ _ _ IHΓ2 S (eimpl (appendEnv Γ1 Γ2) ρ0)).
  constructor.
Qed.

Lemma shift_etvar_lookup_etvar_inv :
  ∀ (r : Ren) (G G' : Env),
       shift_etvar r G G'
       → ∀ x : nat, lookup_etvar G' (r x) -> lookup_etvar G x.
Proof.
intros r Γ1 Γ2 sh. induction sh; intros x lk.
- inversion lk; subst. trivial.
- destruct x; constructor.
  apply IHsh. simpl in *. inversion lk; subst. trivial.
- inversion lk; subst. constructor. auto.
- inversion lk; subst. constructor. auto.
Qed.

Lemma renTy_equal : forall (ρ1 ρ2 : Ty) (r : Ren),
  (forall (x1 x2 : nat), (r x1) = (r x2) -> x1 = x2) ->
  renTy ρ1 r = renTy ρ2 r -> ρ1 = ρ2.
Proof.
intro ρ1. induction ρ1; intros ρ2 r inj eq; destruct ρ2;
simpl in *; inversion eq; f_equal; eauto.
- specialize (IHρ1 ρ2 (ren_up r)). apply IHρ1; auto.
  intros x1 x2 eq'. destruct x1, x2; inversion eq'; auto.
Qed.

Fixpoint impl_vars' (ls : list Ty) : Env :=
  match ls with
  | []     => empty
  | ρ::ls' => eimpl (impl_vars' ls') ρ
  end.

Definition impl_vars (ls : list Ty) : Env :=
  impl_vars' (rev ls).

Lemma appendEnv_impl_vars : forall a b,
  impl_vars (a ++ b) =
  appendEnv (impl_vars a) (impl_vars b).
Proof.
intros a b. revert a. induction b as [ | b' b IHb]; intro a; simpl.
- rewrite app_nil_r. trivial.
- change (b' :: b) with ([b'] ++ b).
  rewrite app_assoc.
  repeat rewrite IHb.
  rewrite <- appendEnv_assoc. simpl. f_equal.
  unfold impl_vars. rewrite rev_app_distr. simpl. trivial.
Qed.

Ltac fix_impl_vars :=
  match goal with
  | [ |- context[impl_vars (?a :: ?l)]] =>
      change (a :: l) with ([a] ++ l);
      rewrite appendEnv_impl_vars; unfold impl_vars;
      simpl; fold (impl_vars l)
  | [ H : context[impl_vars (?a :: ?l)] |- _ ] =>
      change (a :: l) with ([a] ++ l) in H;
      rewrite appendEnv_impl_vars in H; unfold impl_vars in H;
      simpl in H; fold (impl_vars l) in H
  | [ |- context[impl_vars []]] => simpl
  | [ H : context[impl_vars []] |- _ ] => simpl in H
  end.

Lemma weakenIndexTm_impl_vars : forall Σ x,
  weakenIndexTm (impl_vars Σ) x = length Σ + x.
Proof.
intro Σ. induction Σ; intro x; fix_impl_vars; trivial.
rewrite weakenIndexTm_appendEnv.
simpl. rewrite IHΣ, plus_n_Sm. trivial.
Qed.

Lemma weakenTy_impl_vars : forall l ρ,
  weakenTy (impl_vars l) ρ = ρ.
Proof.
intro l. induction l; intro ρ; fix_impl_vars; trivial.
rewrite weakenTy_appendEnv. simpl. trivial.
Qed.

Inductive Sigma_amb_res (Γ : Env) : list Ty ->  F.Tm -> F.Tm -> Prop :=
  | sse_base E : Sigma_amb_res Γ [] E E
  | sse_cons (l : list Ty) ρ' E' E1 E2 :
      amb_res Γ ρ' E' ->
      Sigma_amb_res (eimpl Γ ρ') l E1 E2 ->
      Sigma_amb_res Γ (ρ'::l) E1 (F.subTm E2 F.subt_id (F.snoc F.sub_id E')).

Lemma Sigma_amb_res_amb_res : forall Γ l E1 E2,
  Sigma_amb_res Γ l E1 E2 ->
  forall ρ,
  amb_res (appendEnv Γ (impl_vars l)) (weakenTy (impl_vars l) ρ) E1 ->
  amb_res Γ ρ E2.
Proof.
intros Γ l E1 E2 sse. induction sse; intros ρ ar; simpl in *; trivial.
fix_impl_vars. eapply sub_eimpl_amb_res.
- refine H.
- constructor.
- apply IHsse. rewrite <- appendEnv_assoc in ar.
  rewrite weakenTy_appendEnv in ar. simpl in ar.
  trivial.
Qed.

