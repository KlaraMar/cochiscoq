Require Export Cochis.
Require Export CochisDet.
Require Import List.
Import ListNotations.
Require Export Coq.Logic.FunctionalExtensionality.

(* List Lemmas *)

Lemma RevNil :
  forall A (l: list A), [] = rev l -> l = [].
Proof.
  intros; induction l.
  - auto.
  - exfalso.
    simpl in H.
    apply (app_cons_not_nil (rev l) [] a); auto.
Qed.

Lemma RevAppCons:
  forall A (xs: list A) y ys,
    rev xs ++ y :: ys = rev (y::xs) ++ ys.
Proof.
  intros; simpl;rewrite <- app_assoc; simpl; auto.
Qed.

Lemma RevInj:
  forall A (l1 l2: list A),
     rev l1 = rev l2 -> l1 = l2.
Proof.
  intro; intro; induction l1; intros.
  + symmetry; apply RevNil; auto.
  + induction l2.
      * simpl in H.
        exfalso.
        eapply app_cons_not_nil.
        symmetry; eauto.
      * simpl in H.
        assert (rev l1 = rev l2 /\ a = a0).
        - apply app_inj_tail; auto.
        - destruct H0.
          rewrite H1 in *.
         rewrite (IHl1 l2 H0); reflexivity.
Qed.

Lemma AppIsNil:
  forall A (l1: list A) l2,
     l1 = l2 ++ l1 -> l2 = [].
Proof.
  intros; symmetry; eapply app_inv_tail; rewrite <- app_nil_l in H at 1;eauto.
Qed.

(* Unification Assumptions *)
Axiom unifySTy : nat -> DEnv -> STy -> STy -> SubMT -> Prop.

Axiom unifyEq:
  forall n Γ tau1 tau2 theta,
     unifySTy n Γ tau1 tau2 theta
  -> subSTy tau1 theta = tau2.

Inductive NatDiff: nat -> nat -> nat -> Prop :=
  | NatDiff_here m:      NatDiff m 0 m
  | NatDiff_there n m r:  NatDiff m n r -> NatDiff (S m) (S n) r.

Axiom unifyDomain:
  forall n Γ tau1 tau2 theta,
     unifySTy n Γ tau1 tau2 theta
  -> forall m r, NatDiff m n r -> theta m = mtvar r.

Lemma substZeroDomain:
  forall theta,
  (forall m r, NatDiff m 0 r -> theta m = mtvar r) -> theta = submt_id.
Proof.
  intros;
  apply functional_extensionality; unfold submt_id; intros; apply H; constructor.
Qed.

(* System F Assumptions *)

Definition FTyshift : F.Ty -> F.Ty := fun T => F.renTy T S.

Axiom SS_TApp_id :
  forall T,
    F.subTy (F.renTy T (F.ren_up S)) (F.snoc F.subt_id (F.tvar 0)) = T.

Lemma  foldrenTy: 
  forall T,
    (F.tall (F.renTy T (F.ren_up S))) = F.renTy (F.tall T) S.
Proof. auto.
Qed.

Axiom Typing_Weakening_etvar:
  forall Γ E T,
          F.Typing Γ E T
     -> F.Typing (F.etvar Γ) (F.shiftT E) (F.renTy T S).

Axiom ShiftSubComm:
  forall E theta,
   (F.shift (F.subTm E theta F.sub_id)) = F.subTm (F.shift E) theta F.sub_id.

Axiom ShiftUpExchange:
  forall theta E,
   ( (F.subTm (F.shiftT E) (MtoFSubTy theta) F.sub_id))
   = (F.subTm E (MtoFSubTy (submt_down theta)) F.sub_id).

Axiom SubCTySplit:
  forall theta rho,
      (subCTy rho theta)
  = (subCTy (subCTy rho (submt_up (submt_down theta))) (snoc submt_id (theta 0))).


(* Matching *)

Inductive alg_match : nat -> DEnv -> CTy -> F.Tm -> list CTy -> STy -> F.Tm -> list CTy -> SubMT -> Prop :=
  | alg_m_simp n Γ tau1 tau2 theta E Sigma:
          unifySTy n Γ tau1 tau2 theta
       -> alg_match n Γ (sty tau1) E Sigma tau2 (F.subTm E (MtoFSubTy theta) F.sub_id) (subCTyList Sigma theta) theta
  | alg_m_iapp n Γ rho1 rho2 E1 Sigma1 tau E2 Sigma2 theta:
          alg_match n (deimpl Γ rho1) rho2 (F.app (F.shift E1) (F.var 0)) (rho1::Sigma1) tau E2 Sigma2 theta
       -> alg_match n Γ (ciarr rho1 rho2) E1 Sigma1 tau E2 Sigma2 theta
  | alg_m_tapp n Γ rho E1 Sigma1 tau E2 Sigma2 theta:
          alg_match (S n) (detvar Γ) rho (F.tapp (F.shiftT E1) (F.tvar 0)) (renCTyList Sigma1 S) tau E2 Sigma2 theta
       -> alg_match n Γ (ctall rho) E1 Sigma1 tau E2 Sigma2 (submt_down theta).

(* Preservation of unifyDomain *)

Lemma MatchDomain:
    forall n Γ rho E1 Sigma1 tau E2 Sigma2 theta,
      alg_match n Γ rho E1 Sigma1 tau E2 Sigma2 theta
   -> forall m r, NatDiff m n r -> theta m = mtvar r.
Proof.
  intro; intro; intro; intro; intro; intro; intro; intro; intro; intro H.
  induction H.
  + eapply unifyDomain; eauto.
  + auto.
  + unfold submt_down.
     intros; apply IHalg_match; constructor; auto.
Qed.

(* Preservation of Well-Typing of Tau *)

Inductive BaseEnv : DEnv -> nat -> list CTy -> DEnv -> SubMT -> DEnv -> Prop :=
  | BaseEnv_here Γ theta:
         BaseEnv Γ 0 [] Γ theta Γ
  | BaseEnv_there_impl Γ Γ' Γ'' rho n S theta:
         BaseEnv Γ n S  Γ' theta Γ''
      -> BaseEnv (deimpl Γ rho) n (rho::S)  Γ' theta (deimpl Γ'' (subCTy rho theta))
  | BaseEnv_there_etvar Γ n Si Γ' Γ'' theta:
         BaseEnv Γ n Si Γ' (submt_down theta) Γ''
      -> BaseEnv (detvar Γ) (S n) (renCTyList Si S) Γ' theta Γ''.

Lemma WFT_alg_match_iapp_1 :
   forall n Γ Γ' rho1 rho2 E1 S1 tau E2 S2 theta,
     alg_match n Γ (ciarr rho1 rho2) E1 S1 tau E2 S2 theta
 (* -> BaseEnv Γ n S1 Γ' theta Γ'' *)
  -> wfTy (DEnv2Env Γ') (STy2Ty tau)
  -> wfTy (DEnv2Env Γ') (STy2Ty tau).
Proof. auto.
Qed.

Lemma WFT_alg_match_iapp_2 :
   forall n Γ Γ' rho1 rho2 E1 S1 tau E2 S2 theta Γ'',
     alg_match n Γ (ciarr rho1 rho2) E1 S1 tau E2 S2 theta
  -> BaseEnv Γ n S1 Γ' theta Γ''
  -> BaseEnv (deimpl Γ rho1) n (rho1::S1) Γ' theta (deimpl Γ'' (subCTy rho1 theta)).
Proof.
  intros; constructor; auto.
Qed.

Lemma WFT_alg_match_tapp_1 :
   forall n Γ Γ' rho E1 S1 tau E2 S2 theta,
     alg_match n Γ (ctall rho) E1 S1 tau E2 S2 theta
  -> wfTy (DEnv2Env Γ') (STy2Ty tau)
  -> wfTy (DEnv2Env Γ') (STy2Ty tau).
Proof.
  auto.
Qed.

Lemma WFT_alg_match_tapp_2 :
   forall n Γ Γ' rho E1 S1 tau E2 S2 theta Γ'',
     alg_match n Γ (ctall rho) E1 S1 tau E2 S2 (submt_down theta)
  -> BaseEnv Γ n S1 Γ' (submt_down theta) Γ''
  ->    BaseEnv (detvar Γ) (S n) (renCTyList S1 S) Γ' theta Γ''.
Proof.
  intros; constructor; auto.
Qed.

(* Preservation of Well-Typing of E1 *)
Lemma WT_alg_match_iapp :
   forall n Γ rho1 rho2 E1 S1 tau E2 S2 theta,
     alg_match n Γ (ciarr rho1 rho2) E1 S1 tau E2 S2 theta
  -> F.Typing (CtoFEnv (DEnv2Env Γ)) E1 (CTy2FTy (ciarr rho1 rho2))
  -> F.Typing (CtoFEnv (DEnv2Env (deimpl Γ rho1))) (F.app (F.shift E1) (F.var 0)) (CTy2FTy rho2).
Proof.
  intros.
  simpl.
  eapply F.T_App.
  + unfold CTy2FTy in H0; simpl in H0; fold CTy2FTy.
    apply F.Typing_Weaken_evar; eauto.
  + constructor. constructor.
Qed.

Lemma WT_alg_match_tapp :
   forall n Γ rho E1 S1 tau E2 S2 theta,
     alg_match n Γ (ctall rho) E1 S1 tau E2 S2 theta
  -> F.Typing (CtoFEnv (DEnv2Env Γ)) E1 (CTy2FTy (ctall rho))
  -> F.Typing (CtoFEnv (DEnv2Env (detvar Γ))) (F.tapp (F.shiftT E1) (F.tvar 0)) (CTy2FTy rho).
Proof.
  intros; simpl.
  rewrite <- (SS_TApp_id (CTy2FTy rho)).
  apply F.T_Tapp.
  rewrite foldrenTy.
  apply Typing_Weakening_etvar.
  auto.
Qed.

(* Property of the output algorithmic Sigma (S2) wrt input algorithmic sigma (S1) and declarative sigma (S3) *)
Definition Sigma_Difference (theta: SubMT) (S1: list CTy) (S2: list CTy) (S3: list CTy): Prop :=
  S2 = rev S3 ++ subCTyList S1 theta.

(*
Lemma Sigma_Difference_Nil:
  forall theta Sigma,
        Sigma_Difference theta [] Sigma (rev Sigma).
Proof.
  intros; unfold Sigma_Difference; simpl.
  rewrite app_nil_r.
  rewrite rev_involutive.
  auto.
Qed.
*)

(*
Lemma SigmaDifferenceUnique:
  forall theta S1 S2 S3 S4,
         Sigma_Difference theta S1 S2 S3
    -> Sigma_Difference theta S1 S2 S4
    -> S3 = S4.
Proof.
  intros; unfold Sigma_Difference in *.
  subst S2.
  apply RevInj.
  eapply app_inv_tail.
  eauto.
Qed.
*)

(*
Lemma Sigma_Base:
  forall theta Sigma1 SigmaDiff,
       Sigma_Difference theta Sigma1 (subCTyList Sigma1 theta) SigmaDiff
    -> SigmaDiff = [].
Proof.
  intros.
  unfold Sigma_Difference in H.
  apply RevNil.
  apply (app_inv_tail (subCTyList Sigma1 theta)); auto.
Qed.
*)

Lemma SigmaDifferenceAssoc:
  forall theta rho S1 S2 SDiff,
        Sigma_Difference theta (rho :: S1) S2 SDiff
   -> Sigma_Difference theta S1 S2 (subCTy rho theta :: SDiff).
Proof.
  intros; unfold Sigma_Difference in *.
  simpl; rewrite <- app_assoc.
  simpl; simpl in H.
  auto.
Qed.

Lemma SigmaDifferenceRenSubUp:
  forall theta S1 S2 Sdiff,
         Sigma_Difference theta (renCTyList S1 S) S2 Sdiff
    -> Sigma_Difference (submt_down theta) S1 S2 Sdiff.
Proof.
  induction S1; intros; unfold Sigma_Difference in *; simpl in *.
  + auto.
  + unfold submt_down. admit.
Admitted.

(*
Lemma L0:
  forall A (xs : list A) y z,
    y = z -> y :: xs = z :: xs.
Proof.
  intros; f_equal; auto.
Qed.
*)

Lemma L1 :
  forall theta Si,
   subCTyList (renCTyList Si S) theta = subCTyList Si (submt_down theta).
Proof.
  induction Si.
  + simpl; auto.
  + simpl; rewrite IHSi.
     unfold submt_down.
     f_equal.
    - admit.
Admitted.

Lemma alg_match_SigmaDiff:
  forall n Γ rho E1 Sigma1 tau E2 Sigma2 theta,
      alg_match n Γ rho E1 Sigma1 tau E2 Sigma2 theta
  ->  exists SigmaDiff, Sigma_Difference theta Sigma1 Sigma2 SigmaDiff.
Proof.
  induction 1.
  * exists [].
    unfold Sigma_Difference; simpl; auto.
  * destruct IHalg_match.
    unfold Sigma_Difference in H0.
    exists ((subCTy rho1 theta) :: x).
    unfold Sigma_Difference.
    simpl.
    rewrite <- app_assoc.
    simpl.
    simpl in H0.
    auto.
  * destruct IHalg_match.
    exists x.
   unfold Sigma_Difference in *.
   rewrite L1 in H0; auto.
Qed.

Lemma alg_soundness_match:
  forall n Γ rho E1 Sigma1 tau E2 Sigma2 theta,
      alg_match n Γ  rho E1 Sigma1 tau E2 Sigma2 theta
  -> forall Γsubst Γbase, BaseEnv Γ n Sigma1 Γbase theta  Γsubst
  ->  exists SigmaDiff,
               Sigma_Difference theta Sigma1 Sigma2 SigmaDiff
           /\ DRes_match Γsubst (subCTy rho theta) (F.subTm E1 (MtoFSubTy theta) F.sub_id) tau E2 SigmaDiff.
Proof.
  intro; intro; intro; intro; intro; intro; intro; intro; intro; intro; assert (Hcopy := H); induction H; intro; intro; intro.
  + assert (exists SigmaDiff, Sigma_Difference theta Sigma (subCTyList Sigma theta) SigmaDiff).
     - apply (alg_match_SigmaDiff _ _ _ _ _ _ _ _ _ Hcopy).
     - intros; inversion H1; exists x.
        split; auto.
        simpl.
        rewrite <- (unifyEq n Γ tau1 tau2 theta H).
        unfold Sigma_Difference in H2.
        assert (x = []).
        -- apply RevNil; symmetry.
             apply (AppIsNil _ (subCTyList Sigma theta)); auto.
        -- rewrite H3; apply M_Simp.
  +  assert (IndBaseEnv := WFT_alg_match_iapp_2 _ _ _ _ _ _ _ _ _ _ _ _ Hcopy H0).
       specialize (IHalg_match H _ _ IndBaseEnv).
       inversion IHalg_match; inversion H1.
       exists ((subCTy rho1 theta) :: x).
       split.
      -  apply SigmaDifferenceAssoc; auto.
      - simpl; apply M_Iapp; rewrite ShiftSubComm;auto.
  + assert (IndBaseEnv := WFT_alg_match_tapp_2 _ _ _ _ _ _ _ _ _ _ _ Hcopy H0). 
     specialize (IHalg_match H _ _ IndBaseEnv).
     inversion IHalg_match; inversion H1.
     exists x.
    split.
    - apply SigmaDifferenceRenSubUp; auto.
    - simpl. apply (M_Tapp _ _ (theta 0) ). simpl in H3.
       unfold MtoFSubTy in H3 at 2. rewrite STy2Ty_after_MTy2STy_is_MTy2Ty in H3.
       rewrite <- ShiftUpExchange.
       rewrite <- SubCTySplit.
       apply H3.
Qed.

Inductive alg_focus (alphas: list nat): DEnv -> CTy -> F.Tm -> Prop :=
 | Alg_R_IAbs : forall Γ rho1 rho2 E
              ,  alg_focus alphas (deimpl Γ rho1) rho2 E
              -> alg_focus alphas Γ (ciarr rho1 rho2) (F.abs (CtoFTy (CTy2Ty rho1)) E)
 | Alg_R_TAbs : forall Γ rho E
              , alg_focus (up_tyvars alphas) (detvar Γ) rho E
             -> alg_focus alphas Γ (ctall rho) (F.tabs E)
 | Alg_R_Simp : forall Γ tau E
              , alg_lookup alphas Γ Γ tau E
              -> alg_focus alphas Γ  (sty tau) E
with alg_lookup (alphas: list nat): DEnv -> DEnv -> STy -> F.Tm -> Prop :=
 | Alg_L_RuleMatch: 
             forall Γ Γ' rho tau E E' Sigma theta
             ,  alg_match 0 Γ rho (F.var 0) [] tau E Sigma theta
             -> alg_sigma alphas Γ Sigma E E'
             -> alg_lookup alphas Γ (deimpl Γ' rho) tau E'
 (* TODO: Alg_L_RuleNoMatch *)
 | Alg_L_Var : forall Γ Γ' rho tau E
             , alg_lookup alphas Γ  Γ' tau E
             -> alg_lookup alphas Γ (devar Γ' rho) tau E
 | Alg_L_TVar : forall Γ Γ' tau E
              , alg_lookup alphas Γ  Γ' tau E
              -> alg_lookup alphas Γ (detvar Γ') tau E
with alg_sigma (alphas : list nat): DEnv -> list CTy -> F.Tm -> F.Tm -> Prop :=
  | alg_base Γ E : 
          alg_sigma alphas Γ [] E E
  | alg_cons Γ (ρ : CTy) (l : list CTy) E1 E2 E' : 
          alg_focus alphas Γ ρ E'
       -> alg_sigma alphas Γ l (F.subTm E1 F.subt_id (F.snoc F.sub_id E1)) E2
       -> alg_sigma alphas Γ (ρ::l) E1 E2.

Scheme alg_focus_ind'  := Minimality for alg_focus  Sort Prop
  with alg_lookup_ind' := Minimality for alg_lookup Sort Prop
  with alg_sigma_ind'  := Minimality for alg_sigma  Sort Prop.

Combined Scheme alg_focus_lookup_sigma_ind
  from alg_focus_ind', alg_lookup_ind', alg_sigma_ind'.

Check alg_focus_lookup_sigma_ind.

Definition P  (a: list nat): DEnv -> CTy -> F.Tm -> Prop := DRes_focus a.
Definition P0 (a: list nat): DEnv -> DEnv -> STy -> F.Tm -> Prop := DRes_lookup a.
Definition P1 (a: list nat): DEnv -> list CTy -> F.Tm -> F.Tm -> Prop := Sigma_dres_focus a.

Lemma alg_soundness_focus_lookup_sigma:
  forall (a: list nat),
    (∀ (d : DEnv) (c : CTy) (t : F.Tm), alg_focus a d c t → P a d c t)
                       ∧ (∀ (d d0 : DEnv) (s : STy) (t : F.Tm),
                          alg_lookup a d d0 s t → P0 a d d0 s t)
                         ∧ (∀ (d : DEnv) (l : list CTy) (t t0 : F.Tm),
                            alg_sigma a d l t t0 → P1 a d l t t0).
Proof.
  intro.
  apply (alg_focus_lookup_sigma_ind); unfold P, P0, P1; intros.
  (* focus *)
  - constructor; auto.
  - constructor; auto.
  - constructor; auto.
  (* lookup *)
  -  assert (H2 := alg_soundness_match _ _ _ _ _ _ _ _ _ H _ _ (BaseEnv_here _ _)).
      inversion H2; destruct H3.
      assert (H5 := MatchDomain _ _ _ _ _ _ _ _ _ H).
      assert (H6 := substZeroDomain theta H5).
      subst theta.
      rewrite subCTy_submt_id, MtoFSubTy_submt_id in H4.
      eapply L_Match. 
      eapply H4.
      unfold Sigma_Difference in H3. unfold subCTyList in H3; rewrite app_nil_r in H3; subst Sigma. simpl.
     auto.
   - constructor; auto.
  - constructor; auto.
  (* sigma *)
  - constructor.
  - econstructor.
    * apply H0.
    * auto.
Qed.

Lemma alg_focus_sound :
  forall alphas Γ ρ E,
        alg_focus alphas Γ ρ E
    ->  DRes_focus alphas Γ ρ E.
Proof.
  intros;
  apply alg_soundness_focus_lookup_sigma; auto.
Qed.

Inductive alg_res (Γ : DEnv) (rho: CTy) (E: F.Tm): Prop :=
 | Alg_R_Main : alg_focus (tyvars Γ) Γ rho E 
              -> alg_res Γ rho E.

Lemma alg_sound :
  forall Γ ρ E,
        alg_res Γ ρ E
     -> DRes Γ ρ E.
Proof.
  induction 1;
  auto using R_Main, alg_focus_sound.
Qed.

