# About

This repository contains the proofs of all theorems and lemmata stated in T. Schrijvers et al., "COCHIS: Deterministic and Coherent Implicits" ...

The proofs are partly mechanised in Coq, and partly hand-written. 

The list below refers the reader to the proofs of each theorem / lemma.

# Theorems and proofs

- Theorem 5.1 -> src/Cochis.v l:1201 "Typing_TypePreservation"
- Lemma 5.1 -> src/Cochis.v l:656 "amb_res_TypePreservation"
- Lemma 5.2 -> src/CochisDet.v l:447 "DRes_Soundness"
- Lemma 5.3 -> paper/proof_resolution_determinism.pdf
- Lemma 5.4 -> paper/proof_resolution_coherence.pdf
- Theorem 5.2
    + Soundness -> paper/proof_algorithm_soundness.pdf
    + Completeness -> paper/proof_algorithm_completeness.pdf

